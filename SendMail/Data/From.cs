﻿using System;
namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Class From (Quem esta enviando o email. Relativo ao campo De)
    /// </summary>
    public class From : BaseContact
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.From"/> class.
        /// </summary>
        public From() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.From"/> class.
        /// </summary>
        /// <param name="email">Email.</param>
        public From(string email)
        {
            this.EmailAddress = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.From"/> class.
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="name">Name.</param>
        public From(string email, string name)
        {
            this.EmailAddress = email;
            this.Name = name;
        }
    }
}
