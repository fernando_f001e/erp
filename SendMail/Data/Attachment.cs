﻿using System;
namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Class Attachment.
    /// </summary>
    public class Attachment
    {
        /// <summary>
        /// Gets or sets the name file.
        /// </summary>
        /// <value>The name file.</value>
        public string NameFile { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>The file.</value>
        public byte[] File { get; set; }
    }
}
