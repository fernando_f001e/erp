﻿using System;

namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Email configuration.
    /// </summary>
    public class EmailConfiguration
    {
        /// <summary>
        /// Gets or sets the smtp server. (Domínio ou IP do servidor de envio de e-mail)
        /// </summary>
        /// <value>The smtp server.</value>
        public string SmtpServer { get; set; }

        /// <summary>
        /// Gets or sets the smtp port. (Número da porta do servidor de envio de e-mail)
        /// </summary>
        /// <value>The smtp port.</value>
        public int SmtpPort { get; set; }

        /// <summary>
        /// Gets or sets the smtp username. (Nome de usuário/email que vai autenticar no servidor de envio de e-mail)
        /// </summary>
        /// <value>The smtp username.</value>
        public string SmtpUsername { get; set; }

        /// <summary>
        /// Gets or sets the smtp password. (Senha do usuário que vai autenticar no servidor de envio de e-mail)
        /// </summary>
        /// <value>The smtp password.</value>
        public string SmtpPassword { get; set; }

        /// <summary>
        /// Gets or sets the default email from sender. (E-mail padrão que irá aparecer no campo De do e-mail)
        /// </summary>
        /// <value>The default email from sender.</value>
        public string DefaultEmailFromSender { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:Zeon.Tools.SendMail.Data.EmailConfiguration"/> has certified.
        /// </summary>
        /// <value><c>true</c> if has certified; otherwise, <c>false</c>.</value>
        public bool HasCertified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:Zeon.Tools.SendMail.Data.EmailConfiguration"/> is
        /// authenticated in relay.
        /// </summary>
        /// <value><c>true</c> if is authenticated in relay; otherwise, <c>false</c>.</value>
        public bool IsAuthenticatedInRelay { get; set; }
    }
}
