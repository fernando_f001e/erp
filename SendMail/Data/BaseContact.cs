﻿using System;
namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Class Base contact.
    /// </summary>
    public class BaseContact
    {
        /// <summary>
        /// Gets or sets the name)
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>The email address.</value>
        public string EmailAddress { get; set; }
    }
}
