﻿using System;
using System.Collections.Generic;

namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Class EmailMessage to send email message.
    /// </summary>
    public class EmailMessage
    {
        /// <summary>
        /// Gets or sets from.
        /// (Quem esta enviando o email. Camnpo De)
        /// </summary>
        /// <value>From.</value>
        public From From { get; set; }

        /// <summary>
        /// Gets or sets to. (Destinatários)
        /// </summary>
        /// <value>To.</value>
        public List<To> To { get; set; }

        /// <summary>
        /// Gets or sets the cc.
        /// (Envia com cópia para)
        /// </summary>
        /// <value>The cc.</value>
        public List<Cc> Cc { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// (Assunto do e-mail)
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body message.
        /// (Mensagem que vai no corpo do e-mail)
        /// </summary>
        /// <value>The body message.</value>
        public string BodyMessage { get; set; }

        /// <summary>
        /// Gets or sets the attachments.
        /// (Anexos)
        /// </summary>
        /// <value>The attachments.</value>
        public List<Attachment> Attachments { get; set; }





        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.EmailMessage"/> class.
        /// </summary>
        public EmailMessage()
        {
            this.To = new List<To>();
            this.Cc = new List<Cc>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.EmailMessage"/> class.
        /// </summary>
        /// <param name="listTo">List to.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="body">Body.</param>
        public EmailMessage(List<To> listTo, string subject, string body)
        {
            this.To = listTo;
            this.Cc = new List<Cc>();
            this.Subject = subject;
            this.BodyMessage = body;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.EmailMessage"/> class.
        /// </summary>
        /// <param name="listTo">List to.</param>
        /// <param name="listCc">List cc.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="body">Body.</param>
        public EmailMessage(List<To> listTo, List<Cc> listCc, string subject, string body)
        {
            this.To = listTo;
            this.Cc = listCc;
            this.Subject = subject;
            this.BodyMessage = body;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.EmailMessage"/> class.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="listTo">List to.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="body">Body.</param>
        public EmailMessage(From from, List<To> listTo, string subject, string body)
        {
            this.From = from;
            this.To = listTo;
            this.Cc = new List<Cc>();
            this.Subject = subject;
            this.BodyMessage = body;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.EmailMessage"/> class.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="listTo">List to.</param>
        /// <param name="listCc">List cc.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="body">Body.</param>
        public EmailMessage(From from, List<To> listTo, List<Cc> listCc, string subject, string body)
        {
            this.From = from;
            this.To = listTo;
            this.Cc = listCc;
            this.Subject = subject;
            this.BodyMessage = body;
        }
    }
}
