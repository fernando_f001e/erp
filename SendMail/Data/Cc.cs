﻿using System;
namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Class Cc (Enviar com cópia)
    /// </summary>
    public class Cc : BaseContact
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Tools.SendMail.Data.Cc"/> class.
        /// </summary>
        public Cc() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Tools.SendMail.Data.Cc"/> class.
        /// </summary>
        /// <param name="email">Email.</param>
        public Cc(string email)
        {
            this.EmailAddress = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Tools.SendMail.Data.Cc"/> class.
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="name">Name.</param>
        public Cc(string email, string name)
        {
            this.EmailAddress = email;
            this.Name = name;
        }
    }
}
