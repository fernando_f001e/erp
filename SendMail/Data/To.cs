﻿using System;
namespace FS.Libraries.Tools.SendMail.Data
{
    /// <summary>
    /// Class To (Destinatários).
    /// </summary>
    public class To : BaseContact
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.To"/> class.
        /// </summary>
        public To() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.To"/> class.
        /// </summary>
        /// <param name="email">Email.</param>
        public To(string email)
        {
            this.EmailAddress = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Tools.SendMail.Data.To"/> class.
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="name">Name.</param>
        public To(string email, string name)
        {
            this.EmailAddress = email;
            this.Name = name;
        }
    }
}
