﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FS.Libraries.Tools.SendMail.Data;

namespace FS.Libraries.Tools.SendMail.Services
{
    /// <summary>
    /// Interface ISendMailServices to send mail services.
    /// </summary>
    public interface ISendMailServices
    {
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <returns><c>true</c>, if mail was sent, <c>false</c> otherwise.</returns>
        /// <param name="emailMessage">Email message.</param>
        bool SendEmail(EmailMessage emailMessage);

        /// <summary>
        /// Sends the email. (Send by AWS SES)
        /// </summary>
        /// <returns>The email.</returns>
        /// <param name="from">From.</param>
        /// <param name="toSend">To send.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="html">Html.</param>
        Task<bool> SendEmail(string from, List<string> toSend, string subject, string html);
    }
}
