﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using MimeKit;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FS.Libraries.Tools.SendMail.Data;

namespace FS.Libraries.Tools.SendMail.Services
{
    /// <summary>
    /// Class SendMailServices implement interface ISendMailServices
    /// </summary>
    public class SendMailServices : ISendMailServices
    {

        private EmailConfiguration _emailConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Tools.SendMail.Services.SendMailServices"/> class.
        /// </summary>
        /// <param name="emailConfiguration">Email configuration.</param>
        public SendMailServices(EmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <returns><c>true</c>, if mail was sent, <c>false</c> otherwise.</returns>
        /// <param name="emailMessage">Email message.</param>
        public bool SendEmail(EmailMessage emailMessage)
        {
            Log.Information("Iniciando o envio de email");
            bool result = false;

            if (_emailConfiguration == null)
            {
                throw new Exception("Configuration for sending not informed mail in appsettings.json");
            }

            MimeMessage mimeMessage = new MimeMessage
            {
                Subject = emailMessage.Subject,
                Date = DateTime.Now
            };
            Log.Information("Obtendo dados de envio");

            //--> Validando remetente 
            if (emailMessage.From != null && !string.IsNullOrEmpty(emailMessage.From.EmailAddress))
            {
                if (string.IsNullOrEmpty(emailMessage.From.Name))
                {
                    mimeMessage.From.Add(new MailboxAddress(emailMessage.From.EmailAddress));
                }
                else
                {
                    mimeMessage.From.Add(new MailboxAddress(emailMessage.From.Name, emailMessage.From.EmailAddress));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(_emailConfiguration.DefaultEmailFromSender))
                {
                    mimeMessage.From.Add(new MailboxAddress(_emailConfiguration.DefaultEmailFromSender));
                }
                else
                {
                    throw new Exception("Sender not defined");
                }
            }

            //--> Validando destinatários
            if (emailMessage.To != null && emailMessage.To.Count > 0)
            {
                foreach (To to in emailMessage.To)
                {
                    if (!string.IsNullOrEmpty(to.EmailAddress))
                    {
                        if (!string.IsNullOrEmpty(to.Name))
                        {
                            mimeMessage.To.Add(new MailboxAddress(to.Name, to.EmailAddress));
                        }
                        else
                        {
                            mimeMessage.To.Add(new MailboxAddress(to.EmailAddress));
                        }
                    }
                }
            }
            else
            {
                throw new Exception("No informed recipient");
            }

            //--> Validando envio com cópia
            if (emailMessage.Cc != null && emailMessage.Cc.Count > 0)
            {
                foreach (Cc cc in emailMessage.Cc)
                {
                    if (!string.IsNullOrEmpty(cc.EmailAddress))
                    {
                        if (!string.IsNullOrEmpty(cc.Name))
                        {
                            mimeMessage.To.Add(new MailboxAddress(cc.Name, cc.EmailAddress));
                        }
                        else
                        {
                            mimeMessage.To.Add(new MailboxAddress(cc.EmailAddress));
                        }
                    }
                }
            }

            //--> Validando anexos
            if (emailMessage.Attachments != null && emailMessage.Attachments.Count > 0)
            {
                BodyBuilder builder = new BodyBuilder
                {

                    // Set the plain-text version of the message text
                    TextBody = emailMessage.BodyMessage
                };

                foreach (Data.Attachment attachment in emailMessage.Attachments)
                {
                    if (attachment.File != null && attachment.File.Length > 0)
                    {
                        if (!string.IsNullOrEmpty(attachment.NameFile))
                        {
                            builder.Attachments.Add(attachment.NameFile, attachment.File);

                        }
                        else
                        {
                            builder.Attachments.Add("Anexo", attachment.File);
                        }
                    }
                }

                // Now we just need to set the message body and we're done
                mimeMessage.Body = builder.ToMessageBody();
            }
            else
            {
                mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = emailMessage.BodyMessage
                };
            }

            Log.Information("Obtendo configurações para envio");

            try
            {

                using (MailKit.Net.Smtp.SmtpClient client = new MailKit.Net.Smtp.SmtpClient())
                {
                    Log.Information("Dados Server SMTP");
                    Log.Information("SMTP URL: " + _emailConfiguration.SmtpServer);
                    Log.Information("SMTP PORT: " + _emailConfiguration.SmtpPort.ToString());
                    Log.Information("SMTP USER: " + _emailConfiguration.SmtpUsername);
                    Log.Information("Connectando no SMTP");

                    if (_emailConfiguration.HasCertified)
                    {
                        Log.Information("Validando certificado");
                        System.Net.Security.RemoteCertificateValidationCallback isvalid = client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        if (client.CheckCertificateRevocation)
                        {
                            Log.Information("Com SecureSocketOptions.Auto");
                            client.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, MailKit.Security.SecureSocketOptions.Auto);
                        }
                        else
                        {
                            client.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, MailKit.Security.SecureSocketOptions.None);
                        }
                    }
                    else
                    {
                        client.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, false);
                    }

                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication 
                    // Error 5.5.1 Authentication  
                    if (!_emailConfiguration.IsAuthenticatedInRelay)
                    {
                        Log.Information("Autenticando usuário e senha no servidor SMTP");
                        //client.Authenticate(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);
                        client.Authenticate(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);
                    }
                    else
                    {
                        Log.Information("Authenticação configurada pra ser no relay");
                    }

                    Log.Information("Enviando mensagem");
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                    Log.Information("Desconectando do SMTP");
                    client.Disconnect(true);
                    Log.Information("E-mail enviado!");
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <returns>The email.</returns>
        /// <param name="from">From.</param>
        /// <param name="toSend">To send.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="html">Html.</param>
        public async Task<bool> SendEmail(string from, List<string> toSend, string subject, string html)
        {
            bool result = false;

            try
            {
                toSend = toSend.Distinct().ToList();
                toSend.RemoveAll(x => string.IsNullOrEmpty(x));

                // might want to provide credentials
                using (AmazonSimpleEmailServiceClient ses = new AmazonSimpleEmailServiceClient(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword, RegionEndpoint.USEast1))
                {
                    SendEmailResponse sendResult = await ses.SendEmailAsync(new SendEmailRequest
                    {
                        Source = from,
                        Destination = new Destination(toSend),
                        Message = new Message
                        {
                            Subject = new Content(subject),
                            Body = new Body
                            {
                                Html = new Content(html)
                            }
                        }
                    });

                    result = sendResult.HttpStatusCode == HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

            return result;
        }
    }
}
