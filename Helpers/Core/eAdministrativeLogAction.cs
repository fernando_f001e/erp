﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Common.Helpers
{
    public enum eAdministrativeLogAction
    {
        INSERT = 1,

        UPDATE = 2,

        DELETE = 3
    }
}
