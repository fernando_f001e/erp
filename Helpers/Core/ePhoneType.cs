﻿using System;
namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator for phone type
    /// </summary>
    public enum ePhoneType
    {
        /// <summary>
        /// Indefinido
        /// </summary>
        Indefinable,
        /// <summary>
        /// Residencial
        /// </summary>
        Residential,
        /// <summary>
        /// The mobile. (celular)
        /// </summary>
        Mobile,
        /// <summary>
        /// Comercial
        /// </summary>
        /// 
        Work,
        /// <summary>
        /// Parentes
        /// </summary>
        Familiarity,
        /// <summary>
        /// Vizinhos
        /// </summary>
        Neighbour,
        /// <summary>
        /// Outros
        /// </summary>
        Other
    }
}
