﻿using System;
namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Class ePermissionType
    /// </summary>
    public class ePermissionType
    {
        /// <summary>
        /// Permissão por Grupo
        /// </summary>
        public const string GROUP = "GRP";
        /// <summary>
        /// Permissão por Conta
        /// </summary>
        public const string ACCOUNT = "URS";
    }
}
