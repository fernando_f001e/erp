﻿using System;
namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Class Roles
    /// </summary>
    public class Roles
    {
        /// <summary>
        /// MENU SINISTROS
        /// </summary>
        public const string MENU_SINISTER = "MENU_SINISTER";
        /// <summary>
        /// MENU DISTRIBUIÇÃO
        /// </summary>
        public const string MENU_DISTRIBUTION = "MENU_DISTRIBUTION";
        /// <summary>
        /// MENU IMPORTAÇÕES
        /// </summary>
        public const string MENU_IMPORTS = "MENU_IMPORTS";
        /// <summary>
        /// MENU RELATÓRIOS
        /// </summary>
        public const string MENU_REPORTS = "MENU_REPORTS";
        /// <summary>
        /// MENU MONITORIA
        /// </summary>
        public const string MENU_MONITORING = "MENU_MONITORING";
        /// <summary>
        /// MENU ADMINISTRAÇÃO
        /// </summary>
        public const string MENU_ADMINISTRATION = "MENU_ADMINISTRATION";
        /// <summary>
        /// MENU CONFIGURAÇÕES
        /// </summary>
        public const string MENU_CONFIGURATION = "MENU_CONFIGURATION";

        /// <summary>
        /// ADMINISTRAÇÃO DE CONTAS
        /// </summary>
        public const string RW_ADM_ACCOUNTS = "RW_ADM_ACCOUNTS";
        /// <summary>
        /// ADMINISTRAÇÃO DE GRUPOS
        /// </summary>
        public const string RW_ADM_GROUPS = "RW_ADM_GROUPS";
        /// <summary>
        /// ADMINISTRAÇÃO DE PERMISSÕES
        /// </summary>
        public const string RW_ADM_PERMISSIONS = "RW_ADM_PERMISSIONS";
        /// <summary>
        /// LOGS
        /// </summary>
        public const string RW_ADM_LOGS = "RW_AMD_LOGS";

        /// <summary>
        /// CONFIGURAÇÃO DE AREA
        /// </summary>
        public const string RW_CFG_AREA = "RW_CFG_AREA";
        /// <summary>
        /// CONFIGURAÇÃO DE BANCO
        /// </summary>
        public const string RW_CFG_BANK = "RW_CFG_BANK";
        /// <summary>
        /// CONFIGURAÇÃO DE FERIADOS
        /// </summary>
        public const string RW_CFG_HOLIDAY = "RW_CFG_HOLIDAY";
        /// <summary>
        /// CONFIGURAÇÃO DE FILIAIS
        /// </summary>
        public const string RW_CFG_BRANCH = "RW_CFG_BRANCH";
        /// <summary>
        /// CONFIGURAÇÃO DE SEGURADORAS
        /// </summary>
        public const string RW_CFG_INSURERS = "RW_CFG_INSURERS";
        /// <summary>
        /// CONFIGURAÇÃO DE TIPO DE NEGÓCIO
        /// </summary>
        public const string RW_CFG_BUSINESSTYPE = "RW_CFG_BUSINESSTYPE";
        /// <summary>
        /// CONFIGURAÇÃO DE SEGURADO
        /// </summary>
        public const string RW_CFG_INSURED = "RW_CFG_INSURED";
        /// <summary>
        /// CONFIGURAÇÃO DE REGULADORES (PERITO)
        /// </summary>
        public const string RW_CFG_EXPERT = "RW_CFG_EXPERT";
        /// <summary>
        /// CONFIGURAÇÃO DE REGIÕES DE ATENDIMENTO
        /// </summary>
        public const string RW_CFG_SERVICEREGIONS = "RW_CFG_SERVICEREGIONS";
        /// <summary>
        /// CONFIGURAÇÃO DE ESTÁGIOS
        /// </summary>
        public const string RW_CFG_STAGES = "RW_CFG_STAGES";
        /// <summary>
        /// CONFIGURAÇÃO DE TIPO DE VISTORIA
        /// </summary>
        public const string RW_CFG_REGULATIONTYPE = "RW_CFG_REGULATIONTYPE";
        /// <summary>
        /// CONFIGURAÇÃO DE TIPO DE DOCUMENTOS
        /// </summary>
        public const string RW_CFG_DOCUMENTTYPE = "RW_CFG_DOCUMENTTYPE";
        /// <summary>
        /// CONFIGURAÇÃO DE TIPO DE IMAGENS
        /// </summary>
        public const string RW_CFG_IMAGESTTYPE = "RW_CFG_IMAGESTTYPE";
        /// <summary>
        /// CONFIGURAÇÃO DE SISTEMA DE ORÇAMENTAÇÃO
        /// </summary>
        public const string RW_CFG_BUDGETSYSTEM = "RW_CFG_BUDGETSYSTEM";
        /// <summary>
        /// CONFIGURAÇÃO DE FORMA DE VISTORIA
        /// </summary>
        public const string RW_CFG_SCHEDULINGTYPE = "RW_CFG_SCHEDULINGTYPE";
        /// <summary>
        /// CONFIGURAÇÃO DE TIPO DE CONTATO (AGENDADO COM)
        /// </summary>
        public const string RW_CFG_SCHEDULINGCONTACT = "RW_CFG_SCHEDULINGCONTACT";
        /// <summary>
        /// CONFIGURAÇÃO DE OFICINAS
        /// </summary>
        public const string RW_CFG_WORKSHOP = "RW_CFG_WORKSHOP";
        /// <summary>
        /// CONFIGURAÇÃO DE TIPO DE VEÍCULOS
        /// </summary>
        public const string RW_CFG_AUTOTYPE = "RW_CFG_AUTOTYPE";
        /// <summary>
        /// CONFIGURAÇÃO DE MODELOS DE VEÍCULOS
        /// </summary>
        public const string RW_CFG_AUTOBRAND = "RW_CFG_AUTOBRAND";


        /// <summary>
        /// REGULAÇÃO ABA DE PESQUISA
        /// </summary>
        public const string RW_REG_TABSEARCH = "RW_REG_TABSEARCH";
        /// <summary>
        /// REGULAÇÃO ABA DE DETALHES (LEITURA)
        /// </summary>
        public const string RD_REG_TABDETAILS = "RD_REG_TABDETAILS";
        /// <summary>
        /// REGULAÇÃO ABA DE DETALHES (ESCRITA)
        /// </summary>
        public const string RW_REG_TABDETAILS = "RW_REG_TABDETAILS";
        /// <summary>
        /// REGULAÇÃO ABA DE AGENDAMENTOS (LEITURA)
        /// </summary>
        public const string RD_REG_TABSCHEDULING = "RD_REG_TABSCHEDULING";
        /// <summary>
        /// REGULAÇÃO ABA DE AGENDAMENTOS (ESCRITA)
        /// </summary>
        public const string RW_REG_TABSCHEDULING = "RW_REG_TABSCHEDULING";
        /// <summary>
        /// REGULAÇÃO ABA DE RETORNO OFICINA (LEITURA)
        /// </summary>
        public const string RD_REG_TABWORKSHOP = "RD_REG_TABWORKSHOP";
        /// <summary>
        /// REGULAÇÃO ABA DE RETORNO OFICINA (ESCRITA)
        /// </summary>
        public const string RW_REG_TABWORKSHOP = "RW_REG_TABWORKSHOP";
        /// <summary>
        /// REGULAÇÃO ABA DE REGULAÇÃO (LEITURA)
        /// </summary>
        public const string RD_REG_TABREGULATION = "RD_REG_TABREGULATION";
        /// <summary>
        /// REGULAÇÃO ABA DE REGULAÇÃO (ESCRITA)
        /// </summary>
        public const string RW_REG_TABREGULATION = "RW_REG_TABREGULATION";
        /// <summary>
        /// REGULAÇÃO ABA DE REGULAÇÃO (LEITURA)
        /// </summary>
        public const string RD_REG_TABANALYZE = "RD_REG_TABANALYZE";
        /// <summary>
        /// REGULAÇÃO ABA DE REGULAÇÃO (ESCRITA)
        /// </summary>
        public const string RW_REG_TABANALYZE = "RW_REG_TABANALYZE";

        /// <summary>
        /// REGULAÇÃO ABA DE DIGITAÇÃO (LEITURA)
        /// </summary>
        public const string RD_REG_TABDIGITATION = "RD_REG_TABDIGITATION";
        /// <summary>
        /// REGULAÇÃO ABA DE DIGITAÇÃO (ESCRITA)
        /// </summary>
        public const string RW_REG_TABDIGITATION = "RW_REG_TABDIGITATION";

    }
}
