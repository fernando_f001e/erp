﻿using System;
namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator for measurable quality of the register of the Telephone.
    /// </summary>
    public enum ePhoneQuality
    {
        /// <summary>
        /// Indefinido
        /// </summary>
        Indefinable,
        /// <summary>
        /// Péssimo
        /// </summary>
        VeryBad,
        /// <summary>
        /// Ruim
        /// </summary>
        Bad,
        /// <summary>
        /// Satisfatório
        /// </summary>
        Satisfactory,
        /// <summary>
        /// Bom
        /// </summary>
        Good,
        /// <summary>
        /// Excelente
        /// </summary>
        Excellent
    }
}
