﻿using System;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator to Address Type
    /// </summary>
    public enum eAddressType
    {
        /// <summary>
        /// Indefinido
        /// </summary>
        Indefinable,
        /// <summary>
        /// Residencial
        /// </summary>
        Residential,
        /// <summary>
        /// Comercial
        /// </summary>
        Work,
        /// <summary>
        /// Parentes
        /// </summary>
        Familiarity,
        /// <summary>
        /// Vizinhos
        /// </summary>
        Neighbour,
        /// <summary>
        /// Outros
        /// </summary>
        Other
    }
}
