﻿using System;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator for the nature of the person (physical, Juridical)
    /// </summary>
    public enum eNaturePerson
    {
        /// <summary>
        /// Indefinable person
        /// </summary>
        Indefinable,
        /// <summary>
        /// Physical person
        /// </summary>
        Physics,
        /// <summary>
        /// Juridical person
        /// </summary>
        Juridical
    }
}
