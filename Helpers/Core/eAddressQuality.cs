﻿using System;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator for measurable quality of the register address.
    /// </summary>
    public enum eAddressQuality
    {
        /// <summary>
        /// The indefinable. Indefinido
        /// </summary>
        Indefinable,
        /// <summary>
        /// The very bad. Péssimo
        /// </summary>
        VeryBad,
        /// <summary>
        /// The bad. Ruim
        /// </summary>
        Bad,
        /// <summary>
        /// The satisfactory. Satisfatório
        /// </summary>
        Satisfactory,
        /// <summary>
        /// The good. Bom
        /// </summary>
        Good,
        /// <summary>
        /// The excellent. Excelente
        /// </summary>
        Excellent
    }
}
