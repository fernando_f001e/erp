﻿using System;

namespace FS.Libraries.Common.Helpers
{
    public class MiddlewareResult
    {
        public static JsonReturn<Object> InvalidAppkey()
        {
            //--> Failed (X-AppKey is missing)
            JsonReturn<Object> result = new JsonReturn<Object>();
            result.Code = eCodeReturn.INVALID_APPICATIONKEY;
            result.Data = null;
            result.Message = "X-AppKey is missing";

            return result;
        }

        public static JsonReturn<Object> InvalidMicroservices()
        {
            JsonReturn<Object> result = new JsonReturn<Object>();
            result.Code = eCodeReturn.INVALID_PARAMETER;
            result.Data = null;
            result.Message = "Microservices not found";

            return result;
        }

        public static JsonReturn<Object> Forbidden()
        {
            JsonReturn<Object> result = new JsonReturn<Object>();
            result.Code = eCodeReturn.FORBIDDEN;
            result.Data = null;
            result.Message = "Access denied";

            return result;
        }

        public static JsonReturn<Object> Unauthorized()
        {
            JsonReturn<Object> result = new JsonReturn<Object>();
            result.Code = eCodeReturn.UNAUTHORIZED;
            result.Data = null;
            result.Message = "The request requires user authentication";

            return result;
        }
    }
}
