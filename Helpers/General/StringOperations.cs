﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace FS.Libraries.Common.Helpers
{
	public class StringOperations
	{
		public string RemoveAccents(string text)
		{
				if (!string.IsNullOrEmpty(text))
				{
					text = text.ToLower().Trim();

					string withAccents = "äáâàãéêëèíîïìöóôòõüúûùç";
					string withoutAccents = "aaaaaeeeeiiiiooooouuuuc";

					for (int i = 0; i < withAccents.Length; i++)
					{
						text = text.Replace(withAccents[i].ToString(), withoutAccents[i].ToString());
					}

					return text;
				}

			return string.Empty;
		}

		public string RemoveSpecialCharacters(string text) 
		{
			if (!string.IsNullOrEmpty(text))
			{
				StringBuilder sb = new StringBuilder();
				foreach (char c in text) 
				{
					if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_') 
					{
						sb.Append(c);
					}
				}
				return sb.ToString();
			}

			return string.Empty;
		}

        public string RemoveDiacritics(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public string ReplaceAccents(string text)
        {
            text.Replace('ä', 'a')
                .Replace('á', 'a')
                .Replace('â', 'a')
                .Replace('à', 'a')
                .Replace('ã', 'a')
                .Replace('é', 'e')
                .Replace('ê', 'e')
                .Replace('ë', 'e')
                .Replace('è', 'e')
                .Replace('í', 'i')
                .Replace('î', 'i')
                .Replace('ï', 'i')
                .Replace('ì', 'i')
                .Replace('ö', 'o')
                .Replace('ó', 'o')
                .Replace('ô', 'o')
                .Replace('ò', 'o')
                .Replace('õ', 'o')
                .Replace('ü', 'u')
                .Replace('ú', 'u')
                .Replace('û', 'u')
                .Replace('ù', 'u')
                .Replace('ù', 'u')
                .Replace('ç', 'c');

            return text;
        }
    }
}
