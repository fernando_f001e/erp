﻿using System.Text.RegularExpressions;

namespace FS.Libraries.Common.Helpers
{
    public class CommonAddressBrazil
    {
        private CommonAddressBrazil(string[] value) { Value = value; }

        public string[] Value { get; set; }

        public static string[] AddressBrazil(CommonAddressBrazil commonAddressBrazil)
        {
            return commonAddressBrazil.Value;
        }

        public static CommonAddressBrazil StateAbreviature {
            get {
                return new CommonAddressBrazil(
                    new string[] {  "AC", "AL", "AP", "AM", "BA",
                                    "CE", "DF", "GO", "ES", "MA",
                                    "MT", "MS", "MG", "PA", "PB",
                                    "PR", "PE", "PI", "RJ", "RN",
                                    "RS", "RO", "RR", "SP", "SC",
                                    "SE", "TO", "DF", ""}
                    );
            }
        }
        public static CommonAddressBrazil AddressAbreviature
        {
            get
            {
                return new CommonAddressBrazil(new string[] {
                    //Correios -->
                    "ERA",  "ERA.",     "Aeroporto",
                    "AL",   "AL.",      "Alameda",
                    /*"A",    "A.",*/   "Área",         "Area",
                    "AV",   "AV.",      "AVENIDA",
                    "CPO",  "CPO.",     "Campo",
                    "CHAP", "CHAP.",    "Chácara",
                    "COL",  "COL.",     "Colônia",      "Colonia",
                    "COND", "COND.",    "Condomínio",   "Condominio",
                    "CJ",   "CJ.",      "Conjunto",
                    "DT",   "DT.",      "Distrito",
                    "ESP",  "ESP.",     "Esplanada",
                    "ETC",  "ETC.",     "Estação",      "Estacao",
                    "EST",  "EST.",     "Estrada",
                    "FAV",  "FAV.",     "Favela",
                    "FAZ",  "FAZ.",     "Fazenda",
                    "FRA",  "FRA.",     "Feira",
                    "JD",   "JD.",      "Jardim",
                    "LD",   "LD.",      "Ladeira",
                    "LG",   "LG.",      "Lago",
                    "LGA",  "LGA.",     "Lagoa",
                    "LRG",  "LRG.",     "Largo",
                    "LOT",  "LOT.",     "Loteamento",
                    "MRO",  "MRO.",     "Morro",
                    "NUC",  "NUC.",     "Núcleo",       "Nucleo",
                    "PRQ",  "PRQ.",     "Parque",
                    "PSA",  "PSA.",     "Passarela",
                    "PAT",  "PAT.",     "Pátio",        "Patio",
                    "PC",   "PC.",      "Praça",        "Praca",
                    //"Q",    "Q.",       "Quadra",
                    "REC",  "REC.",     "Recanto",
                    "RES",  "RES.",     "Residencial",
                    "ROD",  "ROD.",     "Rodovia",
                    "R",    "R.",       "RUA",
                    "ST",   "ST.",      "Setor",
                    "SIT",  "SIT.",     "Sítio",        "Sitio",
                    "TV",   "TV.",      "Travessa",
                    "TRC",  "TRC.",     "Trecho",
                    "TRV",  "TRV.",     "Trevo",
                    "VLE",  "VLE.",     "Vale",
                    "VER",  "VER.",     "Vereda",
                    "V",    "V.",       "Via",
                    "VD",   "VD.",      "Viaduto",
                    "VLA",  "VLA.",     "Viela",
                    "VL",   "VL.",      "Vila",
                    //<-- Correios 
                    //DF? -->
                    "SCIA",
                    "SEDF",
                    "SGCV",
                    "SIA",
                    "SMAS",
                    "SOF",
                    "STRC",
                    //<-- DF?
                    ""   } );
            }
        }
    }
}
