﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// File tools.
    /// </summary>
    public class FileTools
    {
        /// <summary>
        /// Gets or sets the environment.
        /// </summary>
        /// <value>The environment.</value>
        public static IHostingEnvironment Environment { get; set; }

        #region Method
        /// <summary>
        /// Saves the file.
        /// </summary>
        /// <returns>The file.</returns>
        /// <param name="path">Path.</param>
        /// <param name="name">Name.</param>
        /// <param name="extension">Extension.</param>
        /// <param name="fileBytes">File bytes.</param>
        /// <param name="toBeOverwrite">If set to <c>true</c> to be overwrite.</param>
        public static string SaveFile(string path, string name, string extension, byte[] fileBytes, bool toBeOverwrite)
        {

            string fullFilePath = string.Empty;

            if (fileBytes != null && fileBytes.Length > 0 && !string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(extension))
            {
                //Verify if the directory exists
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                string fileName = string.Concat(name, extension);
                fullFilePath = Path.Combine(path, fileName);

                //Verify if the file already exists in directory
                //If exists and be locked, a treatment will be realized
                if (!System.IO.File.Exists(fullFilePath) || (toBeOverwrite && !IsFileLocked(fullFilePath)))
                {
                    using var fs = new FileStream(fullFilePath, FileMode.Create);
                    fs.Write(fileBytes, 0, fileBytes.Length);
                    fs.Dispose();
                }
                else
                {
                    //A count of files will be realized in directory, and the number will be addedd to the file's name
                    string partName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                    string newFileName = string.Format("{0}_{1}", name, partName);
                    string newFilePath = Path.Combine(path, string.Concat(newFileName, extension));

                    using (var fs = new FileStream(newFilePath, FileMode.Create))
                    {
                        fs.Write(fileBytes, 0, fileBytes.Length);
                        fs.Dispose();
                    }

                    //Update the path that the file has been saved
                    fullFilePath = newFilePath;

                }

            }

            if (!System.IO.File.Exists(fullFilePath))
            {
                Log.Error(string.Format("FILE NOT SAVED - File: {0}", string.Concat(path, name, extension)));
            }

            return fullFilePath;

        }

        /// <summary>
        /// Save file in the destiny folder
        /// </summary>
        /// <param name="destinyPath"></param>
        /// <param name="fileBytes"></param>
        /// <param name="toBeOverwrite"></param>
        /// <returns></returns>
        public static string SaveFile(string destinyPath, byte[] fileBytes, bool toBeOverwrite)
        {

            string fullFilePath = destinyPath;

            if (fileBytes != null && fileBytes.Length > 0 && !string.IsNullOrEmpty(destinyPath))
            {

                string path = Path.GetDirectoryName(destinyPath);
                string name = Path.GetFileNameWithoutExtension(destinyPath);
                string extension = Path.GetExtension(destinyPath);

                //Verify if the directory exists
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                //Verify if the file already exists in directory
                //If exists and be locked, a treatment will be realized
                if (!System.IO.File.Exists(destinyPath) || (toBeOverwrite && !IsFileLocked(destinyPath)))
                {
                    using var fs = new FileStream(destinyPath, FileMode.Create);
                    fs.Write(fileBytes, 0, fileBytes.Length);
                    fs.Dispose();
                }
                else
                {
                    //A count of files will be realized in directory, and the number will be addedd to the file's name
                    string partName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                    string newFileName = string.Format("{0}_{1}", name, partName);
                    string newFilePath = Path.Combine(path, string.Concat(newFileName, extension));

                    using (var fs = new FileStream(newFilePath, FileMode.Create))
                    {
                        fs.Write(fileBytes, 0, fileBytes.Length);
                        fs.Dispose();
                    }

                    //Update the path that the file has been saved
                    fullFilePath = newFilePath;

                }

            }

            if (!System.IO.File.Exists(destinyPath))
            {
                Log.Error(string.Format("FILE NOT SAVED - File: {0}", destinyPath));
            }

            return fullFilePath;

        }

        /// <summary>
        /// Save file in the destiny folder
        /// </summary>
        /// <param name="destinyPath"></param>
        /// <param name="file"></param>
        /// <param name="toBeOverwrite"></param>
        /// <returns></returns>
        public static string SaveFile(string destinyPath, IFormFile file, bool toBeOverwrite)
        {

            string fullFilePath = destinyPath;

            if (file != null && file.Length > 0 && !string.IsNullOrEmpty(destinyPath))
            {

                string path = Path.GetDirectoryName(destinyPath);
                string name = Path.GetFileNameWithoutExtension(destinyPath);
                string extension = Path.GetExtension(destinyPath);

                //Verify if the directory exists
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                //Get the file's byte array
                byte[] fileBytes;
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    fileBytes = ms.ToArray();
                }

                //Verify if the file already exists in directory. If exists and be locked, a treatment will be realized
                if ((System.IO.File.Exists(destinyPath) && toBeOverwrite) && IsFileLocked(destinyPath))
                {
                    //A count of files will be realized in directory, and the number will be addedd to the file's name
                    string partName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                    string newFileName = string.Format("{0}_{1}", name, partName);
                    string newFilePath = Path.Combine(path, string.Concat(newFileName, extension));

                    //Update the path that the file has been saved
                    destinyPath = newFilePath;

                }

                //Save the physical file
                using var fs = new FileStream(destinyPath, FileMode.Create);
                fs.Write(fileBytes, 0, fileBytes.Length);
                fs.Dispose();

            }

            if (!System.IO.File.Exists(destinyPath))
            {
                Log.Error(string.Format("FILE NOT SAVED - File: {0}", destinyPath));
            }

            return fullFilePath;

        }

        /// <summary>
        /// Save file in the destiny folder
        /// </summary>
        /// <param name="destinyPath"></param>
        /// <param name="fileStream"></param>
        /// <param name="toBeOverwrite"></param>
        /// <returns></returns>
        public static string SaveFile(string destinyPath, Stream fileStream, bool toBeOverwrite)
        {

            string fullFilePath = destinyPath;

            if (fileStream != null && fileStream.Length > 0 && !string.IsNullOrEmpty(destinyPath))
            {

                string path = Path.GetDirectoryName(destinyPath);
                string name = Path.GetFileNameWithoutExtension(destinyPath);
                string extension = Path.GetExtension(destinyPath);

                //Verify if the directory exists
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                //Get the file's byte array
                byte[] fileBytes;
                using (var ms = new MemoryStream())
                {
                    fileStream.CopyTo(ms);
                    fileBytes = ms.ToArray();
                }

                //Verify if the file already exists in directory. If exists and be locked, a treatment will be realized
                if ((toBeOverwrite && System.IO.File.Exists(destinyPath)) && IsFileLocked(destinyPath))
                {
                    //A count of files will be realized in directory, and the number will be addedd to the file's name
                    string partName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                    string newFileName = string.Format("{0}_{1}", name, partName);
                    string newFilePath = Path.Combine(path, string.Concat(newFileName, extension));

                    //Update the path that the file has been saved
                    destinyPath = newFilePath;

                }

                //Save the physical file
                using var fs = new FileStream(destinyPath, FileMode.Create);
                fs.Write(fileBytes, 0, fileBytes.Length);
                fs.Dispose();

            }

            if (!System.IO.File.Exists(destinyPath))
            {
                Log.Error(string.Format("FILE NOT SAVED - File: {0}", destinyPath));
            }

            return fullFilePath;

        }

        /// <summary>
        /// Verify the file's name to remove some forbidden characteres
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string ValidateNameFile(string name)
        {

            StringBuilder sb = new StringBuilder(name);

            if (sb.Length > 0)
            {
                foreach (char character in Path.GetInvalidFileNameChars())
                {
                    sb.Replace(character.ToString(), string.Empty);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Rename the file passed by, and return the complete path
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string RenameFile(string filePath)
        {

            string newName = string.Empty;

            try
            {

                if (!string.IsNullOrEmpty(filePath))
                {
                    string path = Path.GetDirectoryName(filePath);
                    string currentName = Path.GetFileNameWithoutExtension(filePath);
                    string extension = Path.GetExtension(filePath);


                    string partName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");

                    newName = Path.Combine(path, string.Format("{0}_{1}{2}", currentName, partName, extension));
                }

                return newName;

            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error to rename file: {0}", filePath), ex);

                return filePath;
            }

        }

        /// <summary>
        /// Rename the file by adding
        /// </summary>
        /// <returns>The file with count.</returns>
        /// <param name="filePath">File path.</param>
        public static string RenameFileWithCount(string filePath)
        {

            string newName = string.Empty;

            try
            {

                if (!string.IsNullOrEmpty(filePath))
                {
                    string path = Path.GetDirectoryName(filePath);
                    string currentName = Path.GetFileNameWithoutExtension(filePath);
                    string extension = Path.GetExtension(filePath);


                    int partName = Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly).Count() + 1;

                    newName = Path.Combine(path, string.Format("{0}_{1}{2}", currentName, partName, extension));
                }

                return newName;

            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error to rename file: {0}", filePath), ex);

                return filePath;
            }

        }

        /// <summary>
        /// Gets the path temporary.
        /// </summary>
        /// <returns>The path temporary.</returns>
        /// <param name="folders">Folders.</param>
        public static string GetPathTemporary(params object[] folders)
        {

            StringBuilder path = new StringBuilder(Path.Combine(Environment.ContentRootPath, @"Files\Temp"));

            foreach (object folder in folders)
                path.Append(Path.Combine(folder.ToString()));

            if (!Directory.Exists(path.ToString()))
            {
                Directory.CreateDirectory(path.ToString());
            }

            return path.ToString();
        }

        /// <summary>
        /// Gets the path student.
        /// </summary>
        /// <returns>The path student.</returns>
        /// <param name="guid">GUID.</param>
        /// <param name="folder">Folder.</param>
        public static string GetPathStudent(Guid guid, string folder)
        {

            string path = Path.Combine(Environment.ContentRootPath, string.Format(@"Files\Student\{0}\{1}", guid.ToString().ToUpper(), folder));

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        /// <summary>
        /// Gets the path student.
        /// </summary>
        /// <returns>The path student.</returns>
        /// <param name="guidStudent">GUID student.</param>
        /// <param name="folder">Folder.</param>
        /// <param name="guidObj">GUID object.</param>
        public static string GetPathStudent(Guid guidStudent, string folder, Guid guidObj)
        {

            string path = Path.Combine(Environment.ContentRootPath, string.Format(@"Files\Student\{0}\{1}\{2}", guidStudent.ToString().ToUpper(), folder, guidObj.ToString().ToUpper()));

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        /// <summary>
        /// Gets the web path student.
        /// </summary>
        /// <returns>The web path student.</returns>
        /// <param name="guid">GUID.</param>
        /// <param name="folder">Folder.</param>
        public static string GetWebPathStudent(Guid guid, string folder)
        {

            string path = Path.Combine(Environment.WebRootPath, string.Format(@"Files\Student\{0}\{1}", guid.ToString(), folder));

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        /// <summary>
        /// Create a Zip stream and return it
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <returns></returns>
        public static Stream CreateZipStream(string filesFolder)
        {

            using var memoryStream = new MemoryStream();
            using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (string file in Directory.EnumerateFiles(filesFolder, "*", SearchOption.TopDirectoryOnly))
                {
                    zipArchive.CreateEntryFromFile(file, Path.GetFileName(file));
                }
            }

            return memoryStream;
        }

        /// <summary>
        /// Create a Zip stream and return it
        /// </summary>
        /// <param name="listFiles"></param>
        /// <returns></returns>
        public static Stream CreateZipStream(List<FileBody> listFiles)
        {

            MemoryStream memoryStream = new MemoryStream();

            using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (FileBody file in listFiles)
                {
                    var zipEntry = zipArchive.CreateEntry(file.Alias);

                    byte[] fileBytes = System.IO.File.ReadAllBytes(file.UrlPath);

                    using var zipStream = zipEntry.Open();
                    zipStream.Write(fileBytes, 0, fileBytes.Length);
                }
            }

            return memoryStream;
        }

        /// <summary>
        /// Create a Zip file in the project's temporary folder
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="zipName"></param>
        /// <returns></returns>
        public static string CreateZipFile(string filesFolder, string zipName)
        {

            string zipPath = string.Empty;

            try
            {

                zipPath = Path.Combine(GetPathTemporary(), ValidateNameFile(zipName));

                if (!string.IsNullOrEmpty(filesFolder))
                {
                    //Verifies if the zip exists and its locked
                    if (IsFileLocked(zipPath))
                        zipPath = RenameFile(zipPath);

                    using FileStream fs = new FileStream(zipPath, FileMode.Create);
                    using ZipArchive zip = new ZipArchive(fs, ZipArchiveMode.Create);
                    IEnumerable files = new DirectoryInfo(filesFolder).EnumerateFiles("*", SearchOption.TopDirectoryOnly);

                    foreach (string file in files)
                    {
                        zip.CreateEntryFromFile(file, Path.GetFileNameWithoutExtension(file));
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error to create zipfile. Folder: {0} - Exception: {1}", filesFolder, ex.Message));
                return zipPath;
            }

            return zipPath;
        }

        /// <summary>
        /// Try to delete the file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool DeleteFile(string filePath)
        {
            try
            {

                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Can't delete file: {0}", filePath), ex);
                return false;
            }
        }

        /// <summary>
        /// Check if file exists and its locked
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsFileLocked(string file)
        {

            if (System.IO.File.Exists(file))
            {
                FileInfo fileInfo = new FileInfo(file);
                FileStream stream = null;

                try
                {
                    stream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.None);
                }
                catch (IOException)
                {
                    //the file is unavailable because it is:
                    //still being written to
                    //or being processed by another thread
                    //or does not exist (has already been processed)
                    return true;
                }
                finally
                {
                    if (stream != null)
                        stream.Dispose();
                }
            }

            //file is not locked or not exists
            return false;

        }

        /// <summary>
        /// Get Description from Enum Value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescriptionFromEnumValue(Enum value)
        {
            return !(value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() is DescriptionAttribute attribute) ? value.ToString() : attribute.Description;
        }
        
        #endregion
    }
}
