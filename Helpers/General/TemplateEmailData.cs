﻿using System;

namespace FS.Libraries.Common.Helpers
{
    public class TemplateEmailData
    {
        public string Sinister { get; set; }

        public string Workshop { get; set; }

        public string ExpertName { get; set; }

        public string ExpertEmail { get; set; }

        public DateTime? RegulationDate { get; set; }

        public DateTime SchedulingDate { get; set; }
    }
}
