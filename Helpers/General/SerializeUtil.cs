﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace FS.Libraries.Common.Helpers
{
    public static class SerializeUtil
    {
        public static string Serialize(object obj)
        {
            return SerializeJson(obj);
        }

        public static string Deserialize(string obj)
        {
            return DeserializeJson(obj);
        }

        private static string SerializeXml(object obj)
        {
            XmlSerializer xs = new XmlSerializer(obj.GetType());
            using (MemoryStream buffer = new MemoryStream())
            {
                xs.Serialize(buffer, obj);
                return Encoding.ASCII.GetString(buffer.ToArray());
            }
        }

        private static string SerializeJson(object obj)
        {
            var jsonSerializer = new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            var text = new StringBuilder();

            using (var stringWriter = new StringWriter(text))
            using (var textWriter = new JsonTextWriter(stringWriter))
            {
                jsonSerializer.Serialize(textWriter, obj);
            }

            return text.ToString();
        }

        private static string DeserializeJson(string json)
        {
            object obj = JsonConvert.DeserializeObject(json);
            return obj.ToString();
        }

        public static string SerializeXmlObject<T>(T dataObject)
        {
            if (dataObject == null)
            {
                return string.Empty;
            }
            try
            {
                using (StringWriter stringWriter = new System.IO.StringWriter())
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(stringWriter, dataObject);
                    return stringWriter.ToString();
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public static T DeserializeXmlObject<T>(string xml)
             where T : new()
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new T();
            }
            try
            {
                using (var stringReader = new StringReader(xml))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    return (T)serializer.Deserialize(stringReader);
                }
            }
            catch
            {
                return new T();
            }
        }
    }
}
