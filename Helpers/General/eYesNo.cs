﻿using System;
using System.ComponentModel;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator to options Yes or No.
    /// </summary>
    public enum eYesNo
    {
        /// <summary>
        /// Option No, equals is 0
        /// </summary>
        [Description("Não")]
        No,
        /// <summary>
        /// Option Yes, equals is 1
        /// </summary>
        [Description("Sim")]
        Yes
    }
}
