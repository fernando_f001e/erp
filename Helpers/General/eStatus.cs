﻿using System;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Enumerator class to indicate the status of the record.
    /// </summary>
    public enum eStatus
    {
        /// <summary>
        ///  Inactive status (0)
        /// </summary>
        Inactive = 0,
        /// <summary>
        /// Active status (1)
        /// </summary>
        Active = 1
    }
}
