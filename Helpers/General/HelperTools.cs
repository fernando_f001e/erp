﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace FS.Libraries.Common.Helpers
{
    public static class HelperTools
    {

        private static readonly string pathTemplates        = Directory.GetCurrentDirectory();

        public static string EncoderUTF8(string data)
        {
            byte[] bytes = Encoding.Default.GetBytes(data);
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        /// Method to read template xml
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <param name="stringNode"></param>
        /// <returns></returns>
        public static string ReadTemplateXML(string xmlPath, string stringNode)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(xmlPath))
            {
                if (File.Exists(xmlPath))
                {
                    //--> read xml
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(xmlPath);
                    XmlNode node = xmlDoc.SelectSingleNode(stringNode);

                    if (node != null)
                    {
                        result = node.InnerText.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    }
                }
                else throw new Exception("Template not found");
            }

            return result;
        }

        /// <summary>
        /// Get Template's file
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public static string GetPathTemplate(string templateName)
        {
            return Path.Combine(pathTemplates, "Templates", "RegulationAuto", templateName);
        }

    }
}