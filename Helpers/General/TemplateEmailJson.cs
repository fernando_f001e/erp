﻿using System;

namespace FS.Libraries.Common.Helpers
{
    public class TemplateEmailJson
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string Template { get; set; }
    }
}
