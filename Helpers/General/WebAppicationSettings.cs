﻿using System;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Web appication settings.
    /// </summary>
    public class WebAppicationSettings
    {
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string Url { get; set; }
        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>The port.</value>
        public int Port { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Common.Helpers.WebAppicationSettings"/> class.
        /// </summary>
        public WebAppicationSettings() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Common.Helpers.WebAppicationSettings"/> class.
        /// </summary>
        /// <param name="url">URL.</param>
        /// <param name="port">Port.</param>
        public WebAppicationSettings(string url, int port)
        {
            Url = url;
            Port = port;
        }
    }
}
