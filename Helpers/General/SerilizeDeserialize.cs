﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Class SerilizeDeserialize
    /// </summary>
    public class SerilizeDeserialize
    {
        /// <summary>
        /// Serialize collection of any type to a byte stream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] Serialize<T>(T obj)
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter binSerializer = new BinaryFormatter();
                binSerializer.Serialize(memStream, obj);
                return memStream.ToArray();
            }
        }

        /// <summary>
        /// Deserialize collection of any type to a byte stream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedObj"></param>
        /// <returns></returns>
        public static T Deserialize<T>(byte[] serializedObj)
        {
            T obj = default(T);
            using (MemoryStream memStream = new MemoryStream(serializedObj))
            {
                BinaryFormatter binSerializer = new BinaryFormatter();
                obj = (T)binSerializer.Deserialize(memStream);
            }
            return obj;
        }

        /// <summary>
        /// Serialize collection of any type to a byte stream
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static MemoryStream SerializeToStream(object o)
        {
            MemoryStream stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, o);
            return stream;
        }

        /// <summary>
        /// Deserialize collection of any type to a byte stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object DeserializeFromStream(MemoryStream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            object o = formatter.Deserialize(stream);
            return o;
        }

        
    }
}
