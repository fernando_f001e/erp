﻿using System;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// HTTP defines methods (sometimes referred to as verbs) to indicate the desired action to be performed on the identified resource. What this resource represents, whether pre-existing data or data that is generated dynamically, depends on the implementation of the server. Often, the resource corresponds to a file or the output of an executable residing on the server.
    /// </summary>
    public class RequestMethod
    {
        /// <summary>
        /// The HTTP GET method is used to read or retrieve a representation of a resource.
        /// </summary>
        public const string GET = "GET";
        /// <summary>
        /// The POST verb is most often used to create new features
        /// </summary>
        public const string POST = "POST";
        /// <summary>
        /// PUT is most commonly used to replace (or update) resources by executing the request for a known resource URI, with the request body containing the newly updated representation of the original resource.
        /// </summary>
        public const string PUT = "PUT";
        /// <summary>
        /// PATCH is used to partially modify features. The request only needs to contain the specific changes to the resource, not the full resource.
        /// It looks like PUT, but the body contains a set of instructions describing how a resource on the server should be modified to produce a new version.
        /// </summary>
        public const string PATCH = "PATCH";
        /// <summary>
        /// It is used to exclude a resource identified by a URI.
        /// </summary>
        public const string DELETE = "DELETE";
    }
}
