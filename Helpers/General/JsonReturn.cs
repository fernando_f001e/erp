﻿using System;
using System.Collections.Generic;

namespace FS.Libraries.Common.Helpers
{
    public class JsonReturn<T> where T : class, new()
    {
        #region Property
        /// <summary>
        /// Property code returns code details
        /// </summary>
        public eCodeReturn Code { get; set; }

        /// <summary>
        /// Property Message returns message of result
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Property Result returns with lit of object
        /// </summary>
        //public List<T> Result { get; set; }

        /// <summary>
        /// Property Data returns data object (single)sent or generated
        /// </summary>
        public T Data { get; set; }

        public List<T> ListData { get; set; }

        /// <summary>
        /// Property ResultException returns error details 
        /// </summary>
        public Exception ResultException { get; set; }

        #endregion

        //public JsonReturn() => Resources.Helpers.Culture = new System.Globalization.CultureInfo("en-US");

        #region Methods
        /// <summary>
        /// Set return with Success
        /// </summary>
        public void SetSuccess()
        {
            Code = eCodeReturn.SUCCESS;
            Message = "Success";
        }

        /// <summary>
        /// Set return with Success 
        /// </summary>
        /// <param name="obj"></param>
        public void SetSuccess(List<T> obj)
        {
            Code = eCodeReturn.SUCCESS;
            Message = "Success";

            if (obj != null)
            {
                ListData = (List<T>)obj;
            }
        }

        public void SetSuccess(T obj)
        {
            Code = eCodeReturn.SUCCESS;
            Message = "Success";

            if (obj != null)
            {
                Data = (T)obj;
            }
        }

        /// <summary>
        /// Set return  with Success
        /// </summary>
        /// <param name="resultList"></param>
        /*public void SetSuccess(List<T> resultList)
        {
            Code = eCodeReturn.SUCCESS;
            Message = Resources.Helpers.Success;

            if (resultList != null)
            {
                Result = resultList as List<T>;
            }
        }
        */

        /// <summary>
        /// Set return with Invalid parameter
        /// </summary>
        public void SetInvalidParameter()
        {
            SetInvalidParameter(null);
        }

        /// <summary>
        /// Set return with Invalid parameter
        /// </summary>
        /// <param name="obj">You can be informed as Null if you need</param>
        public void SetInvalidParameter(T obj)
        {
            Code = eCodeReturn.INVALID_PARAMETER;
            Message = "Invalid parameter";

            if (obj != null)
            {
                Data = (T)obj;
            }
        }

        /// <summary>
        /// Set return with undefined error
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        public void SetUndefinedError(T obj, string message)
        {
            Code = eCodeReturn.UNDEFINED_ERROR;
            Message = message;

            if (obj != null)
            {
                Data = (T)obj;
            }
        }

        /// <summary>
        /// Set return with action not performed
        /// </summary>
        /// <param name="obj"></param>
        public void SetActionNotPerformed(T obj)
        {
            Code = eCodeReturn.ACTION_NOT_PERFORMED;
            Message = "Action not performed";

            if (obj != null)
            {
                Data = (T)obj;
            }
        }

        /// <summary>
        /// Set return with action not performed
        /// </summary>
        public void SetActionNotPerformed()
        {
            Code = eCodeReturn.ACTION_NOT_PERFORMED;
            Message = "Action not performed";
        }

        /// <summary>
        /// Set return with error in search
        /// </summary>
        /// <param name="obj"></param>
        public void SetErrorSearch(T obj)
        {
            Code = eCodeReturn.SEARCH_ERROR;
            Message = "Error fetching entity";

            if (obj != null)
            {
                Data = (T)obj;
            }
        }

        /// <summary>
        /// Set return with entity not found
        /// </summary>
        /// <param name="obj"></param>
        public void SetNotFound(T obj)
        {
            Code = eCodeReturn.NOTFOUND;
            Message = "Entity not found";

            if (obj != null)
            {
                Data = (T)obj;
            }
        }


        /// <summary>
        /// Set return with info parameter can not null
        /// </summary>
        /// <param name="obj"></param>
        public void SetParmeterNotNull(T obj)
        {
            Code = eCodeReturn.PARAMETER_CAN_NOT_NULL;
            Message = "Parameter submitted can not be null";

            if (obj != null)
            {
                Data = (T)obj;
            }
        }

        /// <summary>
        /// Set return with invalid token
        /// </summary>
        public void SetInvalidToken()
        {
            Code = eCodeReturn.INVALID_TOKEN;
            Message = "Invalid token";
        }

        /// <summary>
        /// Set return with invalid authentication
        /// </summary>
        public void SetInvalidAuthentication()
        {
            Code = eCodeReturn.INVALID_AUTHENTICATION;
            Message = "Invalid authentication";
        }

        /// <summary>
        /// Set return with session expired
        /// </summary>
        public void SetSession()
        {
            Code = eCodeReturn.SESSION_EXPIRED;
            Message = "Session expired";
        }

        public void SetAlreadyRegistered()
        {
            Code = eCodeReturn.NOTACCEPTABLE;
            Message = "Data already registered";
        }

        /// <summary>
        /// Set data Exception to return JSON frontend
        /// </summary>
        /// <param name="ex">It will return only the exception message, for more details, inform the parameter IsShowExceptionDetails as true</param>
        public void SetException(Exception ex)
        {
            SetException(ex, null);
        }

        /// <summary>
        /// Set data Exception to return JSON frontend
        /// </summary>
        /// <param name="ex">It will return only the exception message, for more details, inform the parameter IsShowExceptionDetails as true</param>
        /// <param name="obj">You can be informed as Null if you need</param>
        public void SetException(Exception ex, T obj)
        {
            SetException(ex, obj, false);
        }

        /// <summary>
        /// Set data Exception to return JSON frontend
        /// </summary>
        /// <param name="ex">It will return only the exception message, for more details, inform the parameter IsShowExceptionDetails as true</param>
        /// <param name="obj">You can be informed as Null if you need</param>
        /// <param name="IsShowExceptionDetails">Security Alert: Use only when needed as it will return all information and exception</param>
        public void SetException(Exception ex, T obj, bool IsShowExceptionDetails)
        {
            Code = eCodeReturn.EXCEPTION;
            Message = ex.InnerException.ToString(); //"An exception occurred. Please check the application log";

            if (obj != null)
            {
                Data = (T)obj;
            }

            if (!IsShowExceptionDetails)
            {
                ResultException = new Exception(ex.Message);
            }
            else ResultException = ex;
        }


        #endregion
    }
}
