﻿using System.IO;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// File body.
    /// </summary>
    public class FileBody
    {
        /// <summary>
        /// Gets or sets the entity identifier.
        /// </summary>
        /// <value>The entity identifier.</value>
        public virtual long EntityID { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name { get; set; }
        /// <summary>
        /// Gets or sets the alias.
        /// </summary>
        /// <value>The alias.</value>
        public virtual string Alias { get; set; }
        /// <summary>
        /// Gets or sets the observations.
        /// </summary>
        /// <value>The observations.</value>
        public virtual string Observations { get; set; }
        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        /// <value>The extension.</value>
        public virtual string Extension { get; set; }
        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>The size.</value>
        public virtual long Size { get; set; }
        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        public virtual string ContentType { get; set; }
        /// <summary>
        /// Gets or sets the base64.
        /// </summary>
        /// <value>The base64.</value>
        public virtual string Base64 { get; set; }
        /// <summary>
        /// Gets or sets the URL path.
        /// </summary>
        /// <value>The URL path.</value>
        public virtual string UrlPath { get; set; }
        /// <summary>
        /// Gets or sets the stream.
        /// </summary>
        /// <value>The stream.</value>
        public virtual Stream Stream { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Common.Helpers.FileBody"/> class.
        /// </summary>
        public FileBody()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Common.Helpers.FileBody"/> class.
        /// </summary>
        /// <param name="alias">Alias.</param>
        /// <param name="urlPath">URL path.</param>
        public FileBody(string alias, string urlPath)
        {
            Alias = alias;
            UrlPath = urlPath;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Common.Helpers.FileBody"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="alias">Alias.</param>
        /// <param name="urlPath">URL path.</param>
        public FileBody(string name, string alias, string urlPath)
        {
            Name = name;
            Alias = alias;
            UrlPath = urlPath;
        }

        #endregion

    }
}
