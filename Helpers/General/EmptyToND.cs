﻿using System;
namespace FS.Libraries.Common.Helpers
{
    public class EmptyToND
    {
        public static string Check(string data)
        {
            string result = "N/D";

            if (!string.IsNullOrEmpty(data))
            {
                result = data;
            }

            return result;
        }

        public static Guid CheckGuid(Guid data)
        {
            Guid result = data;

            if (data == null)
            {
                result = Guid.Parse("00000000-0000-0000-0000-000000000000");
            }

            return result;
        }
    }
}

