﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FS.Libraries.Common.Helpers
{
    /// <summary>
    /// Generic class data return of pagination data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonResultSummary<T> where T : class, new()
    {
        #region Property

        /// <summary>
        /// Code returns code details
        /// </summary>
        public eCodeReturn Code { get; set; }

        /// <summary>
        /// Message returns message of result
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Count of data
        /// </summary>
        /// <returns></returns>
        public int Count { get; set; }

        /// <summary>
        /// Total of pages
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Limite of register return
        /// </summary>
        /// <returns></returns>
        public int PagingLimit { get; set; }

        /// <summary>
        /// Return record from index 
        /// </summary>
        /// <returns></returns>
        public int PagingIndex { get; set; }

        /// <summary>
        /// Generic data list return
        /// </summary>
        /// <returns></returns>   
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        /// ResultException returns error details 
        /// </summary>
        public Exception ResultException { get; set; }

        #endregion

        #region Constructor      
        /// <summary>
        /// Constructor of class JsonResultSummary<T>
        /// </summary>
        public JsonResultSummary() { }

        /// <summary>
        ///  Constructor of class JsonResultSummary<T>
        /// </summary>
        /// <param name="dataList"></param>
        public JsonResultSummary(IEnumerable<T> dataList)
        {
            Code = eCodeReturn.SUCCESS;
            Message = "Success";

            if (dataList != null)
            {
                Data = dataList;
                Count = dataList.Count<T>();
            }
        }

        /// <summary>
        /// Constructor of class JsonResultSummary<T>
        /// </summary>
        /// <param name="dataList"></param>
        /// <param name="limit"></param>
        /// <param name="offSet"></param>
        public JsonResultSummary(IEnumerable<T> dataList, int limit, int index, int totalData, int totalPages)
        {
            Code = eCodeReturn.SUCCESS;
            Message = "Success";

            if (dataList != null)
            {
                Data = dataList;
            }
            Count = totalData;
            TotalPages = totalPages;
            PagingLimit = limit;
            PagingIndex = index;
        }

        /// <summary>
        ///  Constructor of class JsonResultSummary<T>
        /// </summary>
        /// <param name="ex"></param>
        public JsonResultSummary(Exception ex)
        {
            SetExecption(ex, false);
        }

        /// <summary>
        ///  Constructor of class JsonResultSummary<T>
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="IsShowExceptionDetails">Security Alert: Use only when needed as it will return all information and exception</param>
        public JsonResultSummary(Exception ex, bool IsShowExceptionDetails)
        {
            SetExecption(ex, IsShowExceptionDetails);
        }

        #endregion

        #region Methods

        private void SetExecption(Exception ex, bool IsShowExceptionDetails)
        {
            Code = eCodeReturn.EXCEPTION;
            Message = "Ops! An exception occurred. Please check the application log";

            if (!IsShowExceptionDetails)
            {
                ResultException = new Exception(ex.Message);
            }
            else ResultException = ex;
        }

        public void SetInvalidToken()
        {
            Code = eCodeReturn.INVALID_TOKEN;
            Message = "Invalid token";
        }

        public void SetNotFound()
        {
            Code = eCodeReturn.NOTFOUND;
            Message = "Entity not found";
        }
        #endregion
    }
}
