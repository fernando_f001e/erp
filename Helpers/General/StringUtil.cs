﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FS.Libraries.Common.Helpers
{
    public static class StringUtil
    {
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static DateTime FirstDayOfThisMonth(DateTime dt)
        {
            DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));

            return firstDayOfThisMonth;
        }

        //retorna o ultimo dia do mes da respectiva data passada por parametro.
        public static string LastDayOfThisMonth(string data)
        {
            DateTime dt = DateTime.Parse(data);
            dt = dt.AddMonths(1);
            DateTime lastDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day));

            return String.Format("{0:yyyy/M/dd}", lastDayOfThisMonth);
        }

        //Valida a data e formata.
        public static bool ValidDate(ref string str)
        {
            try
            {
                DateTime dt = DateTime.Parse(str);
                str = String.Format("{0:yyyy/MM/dd}", dt);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool ValidTime(string thetime)
        {
            Regex checktime = new Regex(@"^([0-2]{1}[0-3]{1}:)?([0-5]{1}\d{1}:){1}([0-5]{1}\d{1}){1}$");
            return checktime.IsMatch(thetime);
        }

        public static string FormatDateHour(string str)
        {
            try
            {
                DateTime dt = DateTime.Parse(str);
                str = String.Format("{0:dd/MM/yyyy hh:mm tt}", dt);
                return str;
            }
            catch
            {
                return "";
            }
        }

        public static string RemoveSpaceAndSpecialCharacter(string value)
        {
            string result = string.Empty;

            string pattern = @"(?i)[^0-9a-záéíóúàèìòùâêîôûãõç\s]";
            Regex reg = new Regex(pattern);
            result = reg.Replace(value, string.Empty);

            result = result.Replace(" ", "_");

            return result;
        }

        //Metodos para substituir o Substring
        public static string MidStart(string valor, int inicio)
        {
            int T = valor.Length;
            string strMid = valor.Substring(inicio - 1);
            return strMid;
        }

        public static string Mid(string valor, int inicio, int quant)
        {
            int T = valor.Length;
            if (T < quant)
            {
                quant = T;
            }
            string strMid = valor.Substring(inicio - 1, quant);
            return strMid;
        }

        public static string StrDup(int count, string s)
        {
            if ((!string.IsNullOrEmpty(s)) && (count > 0))
            {
                if (s.Length > 1)
                {
                    StringBuilder SB = new StringBuilder(s.Length * count);
                    int i = 0;
                    for (i = 0; i <= count - 1; i++)
                    {
                        SB.Append(s);
                    }
                    return SB.ToString();
                }
                else
                {
                    return new string(Convert.ToChar(s), count);
                }
            }
            return null;
        }

        public static string FormateCode(string code, int size = 6)
        {
            string result = null;
            string zero = "0";

            int addZero = size - code.Length;

            for (int i = 0; i < addZero; i++)
            {
                result += zero;
            }

            return result + code;
        }
    }
}
