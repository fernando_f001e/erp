﻿using System;
using System.Security.Claims;

namespace FS.Libraries.Common.Helpers
{
    public class Payload
    {
        /// <summary>
        /// Gets or sets the iss.
        /// </summary>
        /// <value>The iss.</value>
        public string Iss { get; set; }

        /// <summary>
        /// Gets or sets the sub.
        /// </summary>
        /// <value>The sub.</value>
        public string Sub { get; set; }

        /// <summary>
        /// Gets or sets the aud.
        /// </summary>
        /// <value>The aud.</value>
        public string Aud { get; set; }

        /// <summary>
        /// Gets or sets the exp.
        /// </summary>
        /// <value>The exp.</value>
        public DateTime Exp { get; set; }

        /// <summary>
        /// Gets or sets the generation.
        /// </summary>
        /// <value>The generation.</value>
        public DateTime Generation { get; set; }

        /// <summary>
        /// Gets or sets the jti.
        /// </summary>
        /// <value>The jti.</value>
        public Guid Jti { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the avatar.
        /// </summary>
        /// <value>The avatar.</value>
        public string Avatar { get; set; }

        /// <summary>
        /// Gets or sets the account identifier.
        /// </summary>
        /// <value>The account identifier.</value>
        public Guid AccountID { get; set; }

        /// <summary>
        /// Gets or sets the company identifier.
        /// </summary>
        /// <value>The company identifier.</value>
        public Guid CompanyID { get; set; }

        /// <summary>
        /// Gets or sets the group identifier.
        /// </summary>
        /// <value>The group identifier.</value>
        public Guid GroupID { get; set; }

        /// <summary>
        /// Gets or sets the expert identifier.
        /// </summary>
        /// <value>The expert identifier.</value>
        public Guid? ExpertID { get; set; }

        /// <summary>
        /// Gets or sets the mechanical workshop identifier.
        /// </summary>
        /// <value>The mechanical workshop identifier.</value>
        public Guid? MechanicalWorkshopID { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>The role.</value>
        public string Role { get; set; }

        /// <summary>
        /// Gets or sets the is confirmed.
        /// </summary>
        /// <value>The is confirmed.</value>
        public bool IsConfirmed { get; set; }
    }
}
