﻿using System;

namespace ZFS.Libraries.Common.Helpers
{
    /// <summary>
    /// Class helper Token return.
    /// </summary>
    public class TokenReturn
    {
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        public string Token { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Microservices.CoreAPI.Helpers.TokenReturn"/> class.
        /// </summary>
        public TokenReturn() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Microservices.CoreAPI.Helpers.TokenReturn"/> class.
        /// </summary>
        /// <param name="_token">Token.</param>
        public TokenReturn(string _token)
        {
            this.Token = _token;
        }
    }
}
