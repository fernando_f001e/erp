﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.IO;

namespace Infra.ES.GrayLog
{
    public static class GraylogExtensions
    {
        public static void AddGralog(this IServiceCollection services)
        {
            //services.AddSingleton<ILoggerFactory>(s => new SerilogLoggerFactory());
        }

        public static IWebHostBuilder UseGraylog(this IWebHostBuilder builder)
        {
            const string KEY_CONF_RZN_STREAM = "Serilog:Properties:rzn_stream";
            const string KEY_CONF_RZN_SYS = "Serilog:Properties:rzn_sys";

            var _env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";

            var _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{_env}.json", true)
                .AddEnvironmentVariables()
                .Build();

            var rzn_stream = _configuration[KEY_CONF_RZN_STREAM];
            var rzn_sys = _configuration[KEY_CONF_RZN_SYS];

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(_configuration)
                    .Enrich.WithProperty("rzn_env", _env)
                    .Enrich.FromLogContext()
                    .CreateLogger();

            if (Log.Logger == null)
            {
                Log.Logger = new LoggerConfiguration().CreateLogger();
            }

            ValidateRequiredField(KEY_CONF_RZN_STREAM, rzn_stream);
            ValidateRequiredField(KEY_CONF_RZN_SYS, rzn_sys);

            try
            {
                Log.Information("Starting web host");
                //builder.UseSerilog();
                return builder;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                throw;
            }
        }

        private static void ValidateRequiredField(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                var msg = string.Format("The Key {0} cannot be empty. Host terminated unexpectedly", key);
                Log.Fatal(msg);
                throw new InvalidOperationException(msg);
            }
        }
    }
}
