using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FS.Libraries.Eprocurement.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace EprocurementWeb
{
    public class Startup
    {
        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            HostingEnvironment = environment;
            Configuration = configuration;
        }
        public IWebHostEnvironment HostingEnvironment { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string nameMachineName = Environment.MachineName;

            if (HostingEnvironment.IsDevelopment() && !string.IsNullOrEmpty(nameMachineName))
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<EprocurementContext>(options => options.UseNpgsql(Configuration.GetConnectionString(nameMachineName.ToUpper())));
            }
            else
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<EprocurementContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            }

            services.AddControllersWithViews();
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            LogLevel level = Configuration.GetSection("Logging:LogLevel").GetValue<LogLevel>("Default");

            SetLogger(level);

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            RunMigrationUpdateDatabase(app);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Dashboard", action = "Index" });
            });
        }

        private void SetLogger(LogLevel level)
        {
            if (HostingEnvironment.IsDevelopment())
            {
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.FromLogContext()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile(@"Logs/EprocurementAPI.log", retainedFileCountLimit: 7)
                    .WriteTo.Seq("http://localhost:5341")
                    .CreateLogger();
            }
            else
            {
                if (level.Equals(LogLevel.Debug))
                {
                    Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.FromLogContext()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile(@"Logs/EprocurementAPI.log", retainedFileCountLimit: 7)
                    .WriteTo.Seq("http://localhost:5341")
                    .CreateLogger();
                }
                else
                {
                    Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Error()
                        .Enrich.FromLogContext()
                        .WriteTo.LiterateConsole()
                        .WriteTo.RollingFile(@"Logs/EprocurementAPI.log", retainedFileCountLimit: 7)
                        .WriteTo.Seq("http://localhost:5341")
                        .CreateLogger();
                }
            }
        }

        private void RunMigrationUpdateDatabase(IApplicationBuilder app)
        {
            try
            {
                bool migrationsUpdate = Configuration.GetSection("ApplicationConfig").GetValue<bool>("MigrationsDatabaseUpdate");

                if (migrationsUpdate)
                {
                    using IServiceScope serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
                    var contextCore = serviceScope.ServiceProvider.GetService<EprocurementContext>();
                    contextCore.Database.Migrate();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Log.Error("Error Runs the migration update database.", ex);
            }
        }
    }
}
