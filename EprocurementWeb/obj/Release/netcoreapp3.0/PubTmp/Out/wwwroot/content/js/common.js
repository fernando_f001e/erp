﻿var base_url = $('#hdnUrlBase').val();
//var Loading                      = $('#hdnLoading').val();
//var Select                       = $('#hdnSelect').val();
//var NoRecordsRegistered          = $('#hdnNoRecordsRegistered').val();
//var SearchNotFound               = $('#hdnSearchNotFound').val();
//var UseFilter                    = $('#hdnUseFilter').val();
//var searchText                   = $("#hdnSearchRegulation").val();
//var MsgErrorSelectField          = $("#hdnSelectField").val();
//var MsgInsertDataField           = $("#hdnInsertDataField").val();
var errorColor = "#bd362f";
var timeOutError = 5000;
var timeOutWarning = 3000;
var timeOutSucess = 2000;
var timeOutInfo = 7000;
var currFFZoom = 1;
var currIEZoom = 100;
var oTable = "";
var oTableBullarium = "";
var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';
var pageTableAccount = 1;

$(function () {
    /*$('#tableDefault').DataTable({
		"paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });*/
});

function fJS_FormatCode(code, sizeCode) {
	var size = code.length;
	var zero = "";

	if (sizeCode == null || sizeCode == "" || sizeCode == "NaN") {
		sizeCode = 6;
	}

	for (i = size; i < sizeCode; i++) {
		zero += "0";
	}
	return zero.toString() + code.toString();
}

function fJS_ClearForm(name) {
	debugger
	var dataForm = $("#" + name + "").serialize();

	if (dataForm != null) {
		var result = dataForm.split("&");

		for (var i in result) {
			var data = result[i].split("=");
			$("#" + data[0]).val("");
		}
	}
	/*
	$("#" + name + "").each(function ()
	{
		this.reset();
	});
	*/
}

function fJS_ShowWarning(message, data) {
	if (data != null && data != "") {
		$("#" + data[0]).focus();
		$("label[for=" + data[0] + "]").css("color", errorColor);
		$("#" + data[0]).css({ "border-color": errorColor });
	}

	$("#warning-container-message").html(message);
	$("#warning-container")
		.hide()
		.removeClass("hide")
		.slideDown("slow");

	setTimeout(function () {
		$("#warning-container").slideUp("slow");
	}, timeOutWarning);
	$(window).scrollTop(0);
}

function fJS_ShowError(message) {
	$("#error-container-message").html(message);
	$("#error-container")
		.hide()
		.removeClass("hide")
		.slideDown("slow");

	setTimeout(function () {
		$("#error-container").slideUp("slow");
	}, timeOutError);
	$(window).scrollTop(0);
}

function fJS_ShowSucess(message) {
	if (message != null && message != "") {
		$("#sucess-container-message").html(message);
	}

	$("#sucess-container")
		.hide()
		.removeClass("hide")
		.slideDown("slow");

	setTimeout(function () {
		$("#sucess-container").slideUp("slow");
	}, timeOutSucess);

	$(window).scrollTop(0);
}

function fJS_ShowInfo(message) {
	if (message != null && message != "") {
		$("#info-container-message").html(message);
	}

	$("#info-container")
		.hide()
		.removeClass("hide")
		.slideDown("slow");

	setTimeout(function () {
		$("#info-container").slideUp("slow");
	}, timeOutInfo);

	$(window).scrollTop(0);
}

function fJS_IsEmail(email) {
	debugger
	er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
	if (!er.exec(email)) {
		return false;
	}
	else return true;
}

function fJS_IsValidCPF(cpf) {
	var exp = /\.|\-/g
	cpf = cpf.replace(exp, "");
	var digitoDigitado = eval(cpf.charAt(9) + cpf.charAt(10));
	var soma1 = 0, soma2 = 0;
	var vlr = 11;

	for (i = 0; i < 9; i++) {
		soma1 += eval(cpf.charAt(i) * (vlr - 1));
		soma2 += eval(cpf.charAt(i) * vlr);
		vlr--;
	}
	soma1 = (((soma1 * 10) % 11) == 10 ? 0 : ((soma1 * 10) % 11));
	soma2 = (((soma2 + (2 * soma1)) * 10) % 11);

	if (soma1 == 10 || soma1 == 11) soma1 = 0;
	if (soma2 == 10 || soma2 == 11) soma2 = 0;

	var digitoGerado = (soma1 * 10) + soma2;
	if (digitoGerado != digitoDigitado) {
		return false;
	}

	return true;
}

function fJS_IsValidCNPJ(cnpj) {
	var valida = new Array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
	var dig1 = new Number;
	var dig2 = new Number;

	var exp = /\.|\-|\//g
	cnpj = cnpj.toString().replace(exp, "");
	var digito = new Number(eval(cnpj.charAt(12) + cnpj.charAt(13)));

	for (i = 0; i < valida.length; i++) {
		dig1 += (i > 0 ? (cnpj.charAt(i - 1) * valida[i]) : 0);
		dig2 += cnpj.charAt(i) * valida[i];
	}
	dig1 = (((dig1 % 11) < 2) ? 0 : (11 - (dig1 % 11)));
	dig2 = (((dig2 % 11) < 2) ? 0 : (11 - (dig2 % 11)));

	if (((dig1 * 10) + dig2) != digito) {
		return false;
	}

	return true;
}