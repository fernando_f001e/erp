﻿var nLimits = "10";
base_controller = "freight";

$(function () {
    $("#hdnCode").val("");
    $("#btnPrevious").hide();
    $("#btnNext").hide();
});

$("#btnCancel").click(function () {
    fJS_ClearForm("frmFreight")
    $("#hdnCode").val("");
});

$("#btnSave").click(function () {
    var dataForm = $("#frmFreight").serialize();

    var result = dataForm.split("&");
    var message = "";
    var data = "";

    $("label").css("color", "rgb(87,86,85)");

    for (var i in result) {
        var data = result[i].split("=");

        $("#" + data[0]).css({ "border-color": "rgb(204,204,204)" });

        switch (data[0]) {
            case "txtName":
                if (data[1] == "") {
                    message = JSResource.MsgInsertDataField + " " + "<strong>" + $("label[for=" + data[0] + "]").text().replace(':', '') + "</strong>";
                    fJS_ShowWarning(message, data);
                    return false;
                }
                break;
        }
    }

    $.ajax({
        url: base_url + '/' + base_controller + '/add',
        type: 'Post',
        data: dataForm,
        dataType: 'json',
        success: function (response) {
            var obj = response;

            if (obj != null && parseInt(obj.code) == 0) {
                $(window).scrollTop(0);
                $("#hdnCode").val("");
                fJS_ClearForm("frmFreight")
                fJF_LoadGrid(false, false, nLimits);
                fJS_ShowSucess('Operação Realizada com Sucesso !');
            }
            else if (obj != null && parseInt(obj.code) == -9) {
                $(window).scrollTop(0);
                fJS_ShowError(MsgSessionExpired, '');
                setTimeout(function () { window.location = base_url; }, timeOutError);
            }
            else if (obj != null && parseInt(obj.code) == -99) {
                $(window).scrollTop(0);
                fJS_ShowError(obj.message);
            }
            else {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToSave);
            }
            return false;
        },
        error: function (e) {
            $(window).scrollTop(0);
            fJS_ShowError(JSResource.ErrorFailedToSave + " -- " + e.responseText);
            return false;
        }
    });
});

function fJF_GetShowRecords(sel) {
    nLimits = sel.value;
    pageTable = 1;
    fJF_LoadGrid(false, false, nLimits);
    $(window).scrollTop(0);
}

$("#btnSearch").click(function () {
    fJF_LoadGrid(false, false, nLimits);
    $(window).scrollTop(0);
});

$("#btnPrevious").click(function () {
    fJF_LoadGrid(true, false, nLimits);
    $(window).scrollTop(0);
});

$("#btnNext").click(function () {
    fJF_LoadGrid(true, true, nLimits);
    $(window).scrollTop(0);
});

function fJF_LoadGrid(pagination, next, pageLimits) {

    if (pagination) {
        if (next) {
            pageTable++;
        } else {
            pageTable--;
        }
    } else {
        $("#btnNext").show();
    }

    $.ajax({
        url: base_url + '/' + base_controller + '/search',
        type: 'Post',
        data: {
            'pageLimits': parseInt(pageLimits),
            'pageTable': parseInt(pageTable)
        },
        success: function (response) {
            var obj = response;

            if (obj.code == -99) {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + obj.resultException.message);
                return false;
            }
            else {
                var tableDefault = $("#tableDefault");
                if (pageTable >= obj.totalPages) {
                    $("#btnNext").hide();
                }
                if (pageTable < obj.totalPages) {
                    $("#btnNext").show();
                }
                if (pageTable == 1) {
                    $("#btnPrevious").hide();
                } else {
                    $("#btnPrevious").show();
                }

                $("#gridFreight").empty();

                for (var i in obj.data) {
                    var data = obj.data[i].id;
                    var textType = "";
                    var tdStatus = "";
                    var btnAction = "";

                    if (obj.data[i].recordStatus > 0) {
                        textType = "text-default";
                        tdStatus = "<span class='label label-success' title='" + JSResource.Active + "'>" + JSResource.LetterSignActive + "</span>";
                        btnAction = "<a href='javascript:void(0);' onclick='javascript:fJS_Change(" + obj.data[i].id + ",0);' rel='tooltip' title='" + JSResource.InactivateRegister + "'><span class='meta'><span class='icon text-success'><i class='fa fa-check'></i></span></span></a>";
                    }
                    else {
                        textType = "text-muted text-danger";
                        tdStatus = "<span class='label label-danger' title='" + JSResource.Inactive + "'>" + JSResource.LetterSignInactive + "</span>";
                        btnAction = "<a href='javascript:void(0);' onclick='javascript:fJS_Change(" + obj.data[i].id + ",1);' rel='tooltip' title='" + JSResource.ActivateRegister + "'><span class='meta'><span class='icon text-danger'><i class='fa fa-close'></i></span></span></a>";
                    }

                    var tr = $("<tr></tr>");

                    tr.html(('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].id + '</a></td>')
                        + " " + ('<td><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].name + '</a></td>')
                        + " " + ('<td class=text-center><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + tdStatus + '</a></td>')
                        + " " + ('<td class=text-center>' + btnAction + '</td>'));
                    tableDefault.append(tr);
                }
                return false;
            }
        }
    });
}

function fJS_Load(id) {
    if (id != "") {
        $.ajax({
            url: base_url + '/' + base_controller + '/load',
            type: 'Post',
            data: { 'id': parseInt(id) },
            dataType: 'json',
            success: function (response) {
                var obj = response;
                if (obj != null && parseInt(obj.data.id) > 0) {
                    $("#hdnCode").val(obj.data.id);
                    $("#txtName").val(obj.data.name);
                }
                $(window).scrollTop(0);
                return false;
            },
            error: function (e) {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + e.responseText);
                return false;
            }
        });
    }
}

function fJS_Change(id, status) {
    if (id != "") {
        $.ajax({
            url: base_url + '/' + base_controller + '/changestatus',
            type: 'Post',
            data: { 'id': parseInt(id), 'status': parseInt(status) },
            dataType: 'json',
            beforeSend: function () {
                /*$('#modalLoading').modal('show');*/
            },
            success: function (response) {
                $(window).scrollTop(0);
                fJS_ShowSucess('Operação Realizada com Sucesso !');
                fJF_LoadGrid(false, false, nLimits);
                return false;
            },
            error: function (e) {
                /*$('#modalLoading').modal('hide');*/
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToSave + " -- " + e.responseText);
                return false;
            }
        });
    }
}
