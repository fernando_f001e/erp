﻿var nLimits = "10";
var nLimitsSearchModal = "5";
base_controller = "purchaseorder";
base_form = "frmOrder";
var base_form_search = "frmSearch";

$(document).ready(function () {
    $("#hdnCode").val("");
    $("#btnPrevious").hide();
    $("#btnPreviousSearchItem").hide();
    $("#btnNext").hide();
    $("#btnNextSearchItem").hide();
    fJF_LoadAllMeasuredUnit();
});

$("#btnCancel").click(function () {
    fJS_ClearForm(base_form)
    $("#hdnCode").val("");
});

function fJF_GetShowRecordsSearchItem(sel) {
    nLimitsSearchModal = sel.value;
    pageTableAccount = 1;
    fJF_LoadGridSearchItem(false, false, nLimitsSearchModal);
    $(window).scrollTop(0);
}

function fJF_GetShowRecords(sel) {
    nLimits = sel.value;
    pageTableAccount = 1;
    fJF_LoadGrid(false, false, nLimits);
    $(window).scrollTop(0);
}

$("#btnSave").click(function () {
    var dataForm = $("#" + base_form + "").serialize();

    var result = dataForm.split("&");
    var message = "";
    var data = "";

    $("label").css("color", "rgb(87,86,85)");

    for (var i in result) {
        var data = result[i].split("=");

        $("#" + data[0]).css({ "border-color": "rgb(204,204,204)" });

        switch (data[0]) {
            case "txtItemId":
                if (data[1] == "") {
                    message = JSResource.MsgInsertDataField + " " + "<strong>" + $("label[for=" + data[0] + "]").text().replace(':', '') + "</strong>";
                    fJS_ShowWarning(message, data);
                    return false;
                }
                break;
            case "txtQtde":
                if (data[1] == "") {
                    message = JSResource.MsgInsertDataField + " " + "<strong>" + $("label[for=" + data[0] + "]").text().replace(':', '') + "</strong>";
                    fJS_ShowWarning(message, data);
                    return false;
                }
                break;
            case "txtFactor":
                if (data[1] == "") {
                    message = JSResource.MsgInsertDataField + " " + "<strong>" + $("label[for=" + data[0] + "]").text().replace(':', '') + "</strong>";
                    fJS_ShowWarning(message, data);
                    return false;
                }
                break;
            case "cboMeasuredUnit":
                if (data[1] == "" || data[1] == "0") {
                    message = JSResource.MsgInsertDataField + " " + "<strong>" + $("label[for=" + data[0] + "]").text().replace(':', '') + "</strong>";
                    fJS_ShowWarning(message, data);
                    return false;
                }
                break;
        }
    }

    $.ajax({
        url: base_url + '/' + base_controller + '/add',
        type: 'Post',
        data: dataForm,
        dataType: 'json',
        success: function (response) {
            var obj = response;

            if (obj != null && parseInt(obj.code) == 0) {
                $(window).scrollTop(0);
                $("#hdnCode").val("");
                fJS_ClearForm(base_form)
                fJS_ShowSucess('Operação Realizada com Sucesso !');
            }
            else if (obj != null && parseInt(obj.code) == -9) {
                $(window).scrollTop(0);
                fJS_ShowError(MsgSessionExpired, '');
                setTimeout(function () { window.location = base_url; }, timeOutError);
            }
            else if (obj != null && parseInt(obj.code) == -99) {
                $(window).scrollTop(0);
                fJS_ShowError(obj.message);
            }
            else {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToSave);
            }
            return false;
        },
        error: function (e) {
            $(window).scrollTop(0);
            fJS_ShowError(JSResource.ErrorFailedToSave + " -- " + e.responseText);
            return false;
        }
    });
});

function fJF_LoadAllMeasuredUnit() {
    $.ajax({
        url: base_url + '/' + base_controller + '/loadallmeasuredunit',
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            var obj = response;
            var cboMeasuredUnit = $('#cboMeasuredUnit');
            cboMeasuredUnit.find('option').remove();

            var cboSearchMeasuredUnit = $('#cboSearchMeasuredUnit');
            cboSearchMeasuredUnit.find('option').remove();

            $('<option>').val("0").text("").appendTo(cboMeasuredUnit);
            $('<option>').val("0").text("").appendTo(cboSearchMeasuredUnit);

            for (var i in obj.data) {
                $('<option>').val(obj.data[i].id).text(obj.data[i].initial + ' - ' + obj.data[i].name).appendTo(cboMeasuredUnit);
                $('<option>').val(obj.data[i].id).text(obj.data[i].initial + ' - ' + obj.data[i].name).appendTo(cboSearchMeasuredUnit);
            }
            return false;
        },
        error: function (e) {
            $(window).scrollTop(0);
            fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + e.responseText);
            return false;
        }
    });
}

$("#btnSearchItem").click(function () {
    fJF_LoadGridSearchItem(false, false, nLimitsSearchModal);
    $(window).scrollTop(0);
});

$("#btnPreviousSearchItem").click(function () {
    fJF_LoadGridSearchItem(true, false, nLimitsSearchModal);
    $(window).scrollTop(0);
});

$("#btnNextSearchItem").click(function () {
    fJF_LoadGridSearchItem(true, true, nLimitsSearchModal);
    $(window).scrollTop(0);
});

function fJF_LoadGridSearchItem(pagination, next, pageLimits) {

    if (pagination) {
        if (next) {
            pageTableAccount++;
        } else {
            pageTableAccount--;
        }
    } 

    $.ajax({
        url: base_url + '/item/search',
        type: 'Post',
        data: {
            'pageLimits': parseInt(pageLimits),
            'pageTable': parseInt(pageTableAccount),
            'searchName': $("#txtSearchNameItemModal").val()
        },
        success: function (response) {
            var obj = response;
            debugger
            if (obj.code == -99) {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + obj.resultException.message);
                return false;
            }
            else {
                var tableDefault = $("#tableDefaultSearchItem");
                if (pageTableAccount >= obj.totalPages) {
                    $("#btnNextSearchItem").hide();
                }
                if (pageTableAccount < obj.totalPages) {
                    $("#btnNextSearchItem").show();
                }
                if (pageTableAccount == 1) {
                    $("#btnPreviousSearchItem").hide();
                } else {
                    $("#btnPreviousSearchItem").show();
                }

                $("#gridSearchItem").empty();

                for (var i in obj.data) {
                    var data = obj.data[i].id;
                    textType = "text-default";

                    var tr = $("<tr></tr>");

                    tr.html(('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_GetItem(' + data + ')" class="' + textType + '">' + obj.data[i].id + '</a></td>')
                        + " " + ('<td><a href="javascript:void(0);" onclick="javascript:fJS_GetItem(' + data + ')" class="' + textType + '">' + obj.data[i].name + '</a></td>'));
                    tableDefault.append(tr);
                }
                return false;
            }
        }
    });
}

function fJS_GetItem(id) {
    if (id != "") {
        $.ajax({
            url: base_url + '/item/load',
            type: 'Post',
            data: { 'id': parseInt(id) },
            dataType: 'json',
            success: function (response) {
                var obj = response;
                if (obj != null && parseInt(obj.data.id) > 0) {
                    $("#txtItemId").val(obj.data.id);
                    $("#txtName").val(obj.data.name);
                }
                $(window).scrollTop(0);
                $('#modalSearchItem').modal('hide');
                return false;
            },
            error: function (e) {
                $(window).scrollTop(0);
                $('#modalSearchItem').modal('hide');
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + e.responseText);
                return false;
            }
        });
    }
}

function fJF_ShowModalSearchItem() {
    $("#gridSearchItem").empty();
    $('#modalSearchItem').modal('show');
}

$("#btnSearch").click(function () {
    fJF_LoadGrid(false, false, nLimits);
    $(window).scrollTop(0);
});

$("#btnPrevious").click(function () {
    fJF_LoadGrid(true, false, nLimits);
    $(window).scrollTop(0);
});

$("#btnNext").click(function () {
    fJF_LoadGrid(true, true, nLimits);
    $(window).scrollTop(0);
});

function fJF_LoadGrid(pagination, next, pageLimits) {

    if (pagination) {
        if (next) {
            pageTableAccount++;
        } else {
            pageTableAccount--;
        }
    } else {
        $("#btnNext").show();
    }

    $.ajax({
        url: base_url + '/' + base_controller + '/search',
        type: 'Post',
        data: {
            'pageLimits': parseInt(pageLimits),
            'pageTable': parseInt(pageTableAccount),
            'searchNumberOrder': $("#txtSearchNumberOrder").val(),
            'searchItem': $("#txtSearchItem").val(),
            'searchRecordStatus': $("#cboSearchRecordStatus").val()
        },
        success: function (response) {
            var obj = response;

            if (obj.code == -99) {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + obj.resultException.message);
                return false;
            }
            else {
                var tableDefault = $("#tableDefault");
                if (pageTableAccount >= obj.totalPages) {
                    $("#btnNext").hide();
                }
                if (pageTableAccount < obj.totalPages) {
                    $("#btnNext").show();
                }
                if (pageTableAccount == 1) {
                    $("#btnPrevious").hide();
                } else {
                    $("#btnPrevious").show();
                }

                $("#gridOrder").empty();

                for (var i in obj.data) {
                    var data = obj.data[i].id;
                    var textType = "";
                    var tdStatus = "";
                    var btnAction = "";
                    debugger
                    if (obj.data[i].recordStatus > 0) {
                        textType = "text-default";
                        tdStatus = "<span class='label label-success' title='" + JSResource.Active + "'>" + JSResource.LetterSignActive + "</span>";
                        btnAction = "<a href='javascript:void(0);' onclick='javascript:fJS_Change(" + obj.data[i].id + ",0);' rel='tooltip' title='" + JSResource.InactivateRegister + "'><span class='meta'><span class='icon text-success'><i class='fa fa-check'></i></span></span></a>";
                    }
                    else {
                        textType = "text-muted text-danger";
                        tdStatus = "<span class='label label-danger' title='" + JSResource.Inactive + "'>" + JSResource.LetterSignInactive + "</span>";
                        btnAction = "<a href='javascript:void(0);' onclick='javascript:fJS_Change(" + obj.data[i].id + ",1);' rel='tooltip' title='" + JSResource.ActivateRegister + "'><span class='meta'><span class='icon text-danger'><i class='fa fa-close'></i></span></span></a>";
                    }

                    var tr = $("<tr></tr>");

                    tr.html(('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].id + '</a></td>')
                        + " " + ('<td><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].item + '</a></td>')
                        + " " + ('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].qtde + '</a></td>')
                        + " " + ('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].factor + '</a></td>')
                        + " " + ('<td><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].measuredUnit + '</a></td>')
                        + " " + ('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].quotationID + '</a></td>')
                        + " " + ('<td class=text-center><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + tdStatus + '</a></td>')
                        + " " + ('<td class=text-center>' + btnAction + '</td>'));
                    tableDefault.append(tr);
                }
                return false;
            }
        }
    });
}

function fJS_Load(id) {
    if (id != "") {
        $.ajax({
            url: base_url + '/' + base_controller + '/load',
            type: 'Post',
            data: { 'id': parseInt(id) },
            dataType: 'json',
            success: function (response) {
                var obj = response;
                if (obj != null && parseInt(obj.data.id) > 0) {
                    $("#hdnCode").val(obj.data.id);
                    $("#txtItemId").val(obj.data.itemID);
                    $("#txtName").val(obj.data.item.name);
                    $("#txtQtde").val(obj.data.qtde);
                    $("#txtFactor").val(obj.data.factor);
                    $("#cboMeasuredUnit").val(obj.data.measuredUnitID);
                }
                $('[href="#tabCadastro"]').tab('show');
                $(window).scrollTop(0);
                return false;
            },
            error: function (e) {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + e.responseText);
                return false;
            }
        });
    }
}

function fJS_Change(id, status) {
    if (id != "") {
        $.ajax({
            url: base_url + '/' + base_controller + '/changestatus',
            type: 'Post',
            data: { 'id': parseInt(id), 'status': parseInt(status) },
            dataType: 'json',
            beforeSend: function () {
                /*$('#modalLoading').modal('show');*/
            },
            success: function (response) {
                $(window).scrollTop(0);
                fJS_ShowSucess('Operação Realizada com Sucesso !');
                fJF_LoadGrid(false, false, nLimits);
                return false;
            },
            error: function (e) {
                /*$('#modalLoading').modal('hide');*/
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToSave + " -- " + e.responseText);
                return false;
            }
        });
    }
}

