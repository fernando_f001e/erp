﻿var nLimits = "10";
var totalClick = "2";
base_controller = "quotation";
base_form = "frmQuotation";

$(document).ready(function () {
    $("#hdnCode").val("");
    $("#btnPrevious").hide();
    $("#btnNext").hide();
    $("#btnSave").hide();
    $("#btnCancel").hide();
    $("#btnPrevStep").hide();

    $('.nav-tabs > li a[title]').tooltip();

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
    });

    $(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        $active.prev().removeClass('disabled');
        prevTab($active);
    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
    totalClick++;
    if (totalClick > "2") {
        $("#btnPrevStep").show();
    }
    if (totalClick == "5") {
        $("#btnNextStep").hide();
        $("#btnSave").show();
        $("#btnCancel").show();
    }
    prevTabDisable(totalClick);
}

function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
    totalClick--;
    if (totalClick < "5") {
        $("#btnNextStep").show();
        $("#btnSave").hide();
        $("#btnCancel").hide();
    }
    if (totalClick == "2") {
        $("#btnPrevStep").hide();
    }
    nextTabDisable(totalClick);
}

function nextTabDisable(ii) {
    var $active = $('.wizard .nav-tabs li.active');
    for (var i = 1; i <= ii; i++) {
        $active.next().addClass('disabled');
    }
}

function prevTabDisable(ii) {
    var $active = $('.wizard .nav-tabs li.active');
    for (var i = 1; i <= ii; i++) {
        $active.prev().addClass('disabled');
    }
}

function fJF_GetShowRecordsOrder(sel) {
    nLimits = sel.value;
    pageTableAccount = 1;
    fJF_LoadGridOrder(false, false, nLimits);
    $(window).scrollTop(0);
}

$("#btnSearchOrder").click(function () {
    fJF_LoadGridOrder(false, false, nLimits);
    $(window).scrollTop(0);
});

$("#btnPreviousOrder").click(function () {
    fJF_LoadGridOrder(true, false, nLimits);
    $(window).scrollTop(0);
});

$("#btnNextOrder").click(function () {
    fJF_LoadGridOrder(true, true, nLimits);
    $(window).scrollTop(0);
});

function fJF_LoadGridOrder(pagination, next, pageLimits) {

    if (pagination) {
        if (next) {
            pageTableAccount++;
        } else {
            pageTableAccount--;
        }
    } else {
        $("#btnNextOrder").show();
    }

    $.ajax({
        url: base_url + '/purchaseorder/search',
        type: 'Post',
        data: {
            'pageLimits': parseInt(pageLimits),
            'pageTable': parseInt(pageTableAccount),
            'searchNumberOrder': $("#txtSearchNumberOrder").val(),
            'searchItem': $("#txtSearchItem").val(),
            'searchRecordStatus': $("#cboSearchRecordStatus").val()
        },
        success: function (response) {
            var obj = response;

            if (obj.code == -99) {
                $(window).scrollTop(0);
                fJS_ShowError(JSResource.ErrorFailedToLoad + " -- " + obj.resultException.message);
                return false;
            }
            else {
                var tableDefault = $("#tableDefaultOrder");
                if (pageTableAccount >= obj.totalPages) {
                    $("#btnNextOrder").hide();
                }
                if (pageTableAccount < obj.totalPages) {
                    $("#btnNextOrder").show();
                }
                if (pageTableAccount == 1) {
                    $("#btnPreviousOrder").hide();
                } else {
                    $("#btnPreviousOrder").show();
                }

                $("#gridOrder").empty();

                for (var i in obj.data) {
                    var data = obj.data[i].id;
                    var textType = "text-default";
                    
                    var tr = $("<tr></tr>");

                    tr.html(('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].id + '</a></td>')
                        + " " + ('<td><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].item + '</a></td>')
                        + " " + ('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].qtde + '</a></td>')
                        + " " + ('<td class=text-right><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].factor + '</a></td>')
                        + " " + ('<td><a href="javascript:void(0);" onclick="javascript:fJS_Load(' + data + ')" class="' + textType + '">' + obj.data[i].measuredUnit + '</a></td>'));
                    tableDefault.append(tr);
                }
                return false;
            }
        }
    });
}
