﻿
namespace FS.WebApplication.EprocurementWeb.Helpers
{
    public class PathViews
    {
        public static string Login = "~/Views/Authentication/Login.cshtml";
        public static string Dashboard = "~/Views/Common/Dashboard.cshtml";
        public static string Account = "~/Views/Administration/Account.cshtml";
        public static string PaymentCondition = "~/Views/Common/PaymentCondition.cshtml";
        public static string Freight = "~/Views/Common/Freight.cshtml";
        public static string Coin = "~/Views/Common/Coin.cshtml";
        public static string MeasuredUnit = "~/Views/Common/MeasuredUnit.cshtml";
        public static string Item = "~/Views/Common/Item.cshtml";
        public static string Supplier = "~/Views/Common/Supplier.cshtml";
        public static string NewOrder = "~/Views/Order/PurchaseOrder.cshtml";
        public static string Quotation = "~/Views/Quotation/Quotation.cshtml";
    }
}
