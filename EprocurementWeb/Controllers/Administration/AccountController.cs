﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class AccountController : ControllerBase
    {
        public AccountController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.Account);
        }

        [HttpPost]
        public JsonResultSummary<Account> Search()
        {
            JsonResultSummary<Account> summary;
            AccountInputFilter accountInput = new AccountInputFilter
            {
                Name = Request.Form["searchName"],
                Email = Request.Form["searchEmail"],
                Type = string.IsNullOrEmpty(Request.Form["type"]) ? 0 : Convert.ToInt32(Request.Form["type"])
            };

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<Account> result = IProxyServices.Eprocurement.Account.GetAllCRUD(accountInput, inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<Account>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<Account>(ex);
                Log.Error(ex, "Error save Account)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<Account> Add()
        {
            Account obj = null;
            JsonReturn<Account> result = new JsonReturn<Account>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.Account.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new Account();

                obj.Name = Request.Form["txtName"];
                obj.Email = Request.Form["txtEmail"];
                obj.Type = Convert.ToInt32(Request.Form["cboType"]);

                if (obj.ID == 0)
                {
                    obj.Password = IProxyServices.CryptoServices.GetSHA512(Request.Form["txtPassword"]);
                    obj.RecordStatus = (int)eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Account.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Account.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save Account)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Account> Load()
        {
            Account obj = new Account();
            JsonReturn<Account> result = new JsonReturn<Account>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.Account.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load Account)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Account> ChangeStatus()
        {
            Account obj = null;
            JsonReturn<Account> result = new JsonReturn<Account>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.Account.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (int)(eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.Account.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus Account)");
            }
            return result;
        }
    }
}
