﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class ItemController : ControllerBase
    {
        public ItemController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.Item);
        }

        [HttpPost]
        public JsonResultSummary<ItemViewModel> Search()
        {
            JsonResultSummary<ItemViewModel> summary;
            ItemInputFilter itemInput = new ItemInputFilter
            {
                CompanyId = CompanyLogged.ID,
                Name = Request.Form["searchName"],
                MeasuredUnitId = string.IsNullOrEmpty(Request.Form["searchMeasuredUnit"]) ? 0 : Convert.ToInt32(Request.Form["searchMeasuredUnit"])
            };

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<ItemViewModel> result = IProxyServices.Eprocurement.Item.GetAllCRUD(itemInput, inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<ItemViewModel>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<ItemViewModel>(ex);
                Log.Error(ex, "Error save Item)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<Item> Add()
        {
            Item obj = null;
            JsonReturn<Item> result = new JsonReturn<Item>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.Item.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new Item();

                obj.CompanyID = CompanyLogged.ID;
                obj.Name = Request.Form["txtName"];
                obj.MeasuredUnitID = Convert.ToInt32(Request.Form["cboMeasuredUnit"]);

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Item.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Item.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save Item)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Item> Load()
        {
            Item obj = new Item();
            JsonReturn<Item> result = new JsonReturn<Item>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.Item.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load Item)");
            }
            return result;
        }

        [HttpGet]
        public JsonResultSummary<MeasuredUnit> LoadAllMeasuredUnit()
        {
            JsonResultSummary<MeasuredUnit> summary;

            try
            {
                IEnumerable<MeasuredUnit> result = IProxyServices.Eprocurement.MeasuredUnit.GetAll();
                summary = new JsonResultSummary<MeasuredUnit>(result);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<MeasuredUnit>(ex);
                Log.Error(ex, "Error LoadAllMeasuredUnit)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<Item> ChangeStatus()
        {
            Item obj = null;
            JsonReturn<Item> result = new JsonReturn<Item>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.Item.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.Item.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus Item)");
            }
            return result;
        }
    }
}
