﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class PurchaseOrderController : ControllerBase
    {
        public PurchaseOrderController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.NewOrder);
        }

        [HttpPost]
        public JsonResultSummary<PurchaseOrderViewModel> Search()
        {
            JsonResultSummary<PurchaseOrderViewModel> summary;
            PurchaseOrderInputFilter itemInput = new PurchaseOrderInputFilter
            {
                CompanyId = CompanyLogged.ID,
                AccountId = AccountLogged.ID,
                Id = string.IsNullOrEmpty(Request.Form["searchNumberOrder"]) ? 0 : Convert.ToInt32(Request.Form["searchNumberOrder"]),
                Name = Request.Form["searchItem"],
                RecodStatus = string.IsNullOrEmpty(Request.Form["searchRecordStatus"]) ? 0 : Convert.ToInt32(Request.Form["searchRecordStatus"]),
            };

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<PurchaseOrderViewModel> result = IProxyServices.Eprocurement.PurchaseOrder.GetAllCRUD(itemInput, inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<PurchaseOrderViewModel>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<PurchaseOrderViewModel>(ex);
                Log.Error(ex, "Error save PurchaseOrderViewModel)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<PurchaseOrder> Add()
        {
            PurchaseOrder obj = null;
            JsonReturn<PurchaseOrder> result = new JsonReturn<PurchaseOrder>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.PurchaseOrder.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new PurchaseOrder();

                obj.CompanyID = CompanyLogged.ID;
                obj.AccountID = AccountLogged.ID;
                obj.ItemID = Convert.ToInt32(Request.Form["txtItemId"]);
                obj.Qtde = Convert.ToDecimal(Request.Form["txtQtde"]);
                obj.Factor = Convert.ToInt32(Request.Form["txtFactor"]);
                obj.MeasuredUnitID = Convert.ToInt32(Request.Form["cboMeasuredUnit"]);

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.PurchaseOrder.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.PurchaseOrder.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save PurchaseOrder)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<PurchaseOrder> Load()
        {
            PurchaseOrder obj = new PurchaseOrder();
            JsonReturn<PurchaseOrder> result = new JsonReturn<PurchaseOrder>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.PurchaseOrder.FindBy(t => t.ID ==  Convert.ToInt32(Request.Form["id"]), t => t.Item);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load PurchaseOrder)");
            }
            return result;
        }

        [HttpGet]
        public JsonResultSummary<MeasuredUnit> LoadAllMeasuredUnit()
        {
            JsonResultSummary<MeasuredUnit> summary;

            try
            {
                IEnumerable<MeasuredUnit> result = IProxyServices.Eprocurement.MeasuredUnit.GetAll();
                summary = new JsonResultSummary<MeasuredUnit>(result);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<MeasuredUnit>(ex);
                Log.Error(ex, "Error Load AllPurchaseOrder)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<PurchaseOrder> ChangeStatus()
        {
            PurchaseOrder obj = null;
            JsonReturn<PurchaseOrder> result = new JsonReturn<PurchaseOrder>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.PurchaseOrder.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.PurchaseOrder.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus PurchaseOrder)");
            }
            return result;
        }
    }
}
