﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class SupplierController : ControllerBase
    {
        public SupplierController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.Supplier);
        }

        public JsonResultSummary<Supplier> Search()
        {
            JsonResultSummary<Supplier> summary;
            SupplierInputFilter itemInput = new SupplierInputFilter
            {
                Cnpj = Request.Form["searchCnpj"],
                Name = Request.Form["searchName"]
            };

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<Supplier> result = IProxyServices.Eprocurement.Supplier.GetAllCRUD(itemInput, inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<Supplier>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<Supplier>(ex);
                Log.Error(ex, "Error Search Supplier)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<Supplier> Add()
        {
            Supplier obj = null;
            JsonReturn<Supplier> result = new JsonReturn<Supplier>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.Supplier.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new Supplier();

                obj.Cnpj = Request.Form["txtCnpj"];
                obj.Name = Request.Form["txtName"];

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Supplier.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Supplier.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error add Supplier)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Supplier> Load()
        {
            Supplier obj = new Supplier();
            JsonReturn<Supplier> result = new JsonReturn<Supplier>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.Supplier.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load Supplier)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Supplier> ChangeStatus()
        {
            Supplier obj = null;
            JsonReturn<Supplier> result = new JsonReturn<Supplier>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.Supplier.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.Supplier.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus Supplier)");
            }
            return result;
        }
    }
}
