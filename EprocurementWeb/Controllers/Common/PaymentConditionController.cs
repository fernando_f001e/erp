﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class PaymentConditionController : ControllerBase
    {
        public PaymentConditionController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.PaymentCondition);
        }

        [HttpPost]
        public JsonResultSummary<PaymentCondition> Search()
        {
            JsonResultSummary<PaymentCondition> summary;

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<PaymentCondition> result = IProxyServices.Eprocurement.PaymentCondition.GetAllCRUD(inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<PaymentCondition>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<PaymentCondition>(ex);
                Log.Error(ex, "Error Search PaymentCondition)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<PaymentCondition> Add()
        {
            PaymentCondition obj = null;
            JsonReturn<PaymentCondition> result = new JsonReturn<PaymentCondition>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.PaymentCondition.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new PaymentCondition();

                obj.Description = Request.Form["txtDescription"];

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.PaymentCondition.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.PaymentCondition.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save Account)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<PaymentCondition> Load()
        {
            PaymentCondition obj = new PaymentCondition();
            JsonReturn<PaymentCondition> result = new JsonReturn<PaymentCondition>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.PaymentCondition.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load PaymentCondition)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<PaymentCondition> ChangeStatus()
        {
            PaymentCondition obj = null;
            JsonReturn<PaymentCondition> result = new JsonReturn<PaymentCondition>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.PaymentCondition.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.PaymentCondition.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus PaymentCondition)");
            }
            return result;
        }
    }
}
