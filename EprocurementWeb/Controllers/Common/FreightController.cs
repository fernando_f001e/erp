﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class FreightController : ControllerBase
    {
        public FreightController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.Freight);
        }
        [HttpPost]
        public JsonResultSummary<Freight> Search()
        {
            JsonResultSummary<Freight> summary;

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<Freight> result = IProxyServices.Eprocurement.Freight.GetAllCRUD(inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<Freight>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<Freight>(ex);
                Log.Error(ex, "Error Search Freight)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<Freight> Add()
        {
            Freight obj = null;
            JsonReturn<Freight> result = new JsonReturn<Freight>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.Freight.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new Freight();

                obj.Name = Request.Form["txtName"];

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Freight.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Freight.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save Freight)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Freight> Load()
        {
            Freight obj = new Freight();
            JsonReturn<Freight> result = new JsonReturn<Freight>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.Freight.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load Freight)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Freight> ChangeStatus()
        {
            Freight obj = null;
            JsonReturn<Freight> result = new JsonReturn<Freight>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.Freight.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.Freight.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus Freight)");
            }
            return result;
        }
    }
}
