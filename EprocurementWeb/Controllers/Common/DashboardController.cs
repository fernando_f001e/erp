﻿using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FS.WebApplication.EprocurementWeb.Controllers.Common
{
    public class DashboardController : Controller
    {
        private readonly ILogger<DashboardController> _logger;

        public DashboardController(ILogger<DashboardController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(PathViews.Dashboard);
        }
    }
}
