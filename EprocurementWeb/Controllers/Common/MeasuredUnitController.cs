﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class MeasuredUnitController : ControllerBase
    {
        public MeasuredUnitController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.MeasuredUnit);
        }
        [HttpPost]
        public JsonResultSummary<MeasuredUnit> Search()
        {
            JsonResultSummary<MeasuredUnit> summary;

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<MeasuredUnit> result = IProxyServices.Eprocurement.MeasuredUnit.GetAllCRUD(inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<MeasuredUnit>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<MeasuredUnit>(ex);
                Log.Error(ex, "Error Search MeasuredUnit)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<MeasuredUnit> Add()
        {
            MeasuredUnit obj = null;
            JsonReturn<MeasuredUnit> result = new JsonReturn<MeasuredUnit>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.MeasuredUnit.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new MeasuredUnit();

                obj.Name = Request.Form["txtName"];
                obj.Initial = Request.Form["txtInitial"];

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.MeasuredUnit.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.MeasuredUnit.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save MeasuredUnit)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<MeasuredUnit> Load()
        {
            MeasuredUnit obj = new MeasuredUnit();
            JsonReturn<MeasuredUnit> result = new JsonReturn<MeasuredUnit>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.MeasuredUnit.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load MeasuredUnit)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<MeasuredUnit> ChangeStatus()
        {
            MeasuredUnit obj = null;
            JsonReturn<MeasuredUnit> result = new JsonReturn<MeasuredUnit>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.MeasuredUnit.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.MeasuredUnit.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus MeasuredUnit)");
            }
            return result;
        }
    }
}
