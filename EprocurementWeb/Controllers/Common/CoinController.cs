﻿using FS.Libraries.Common.Helpers;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;
using FS.Libraries.Proxy.Services;
using FS.WebApplication.EprocurementWeb.Helpers;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class CoinController : ControllerBase
    {
        public CoinController(EprocurementContext eprocurementContext) : base(eprocurementContext) { }

        public IActionResult Index()
        {
            return View(PathViews.Coin);
        }
        [HttpPost]
        public JsonResultSummary<Coin> Search()
        {
            JsonResultSummary<Coin> summary;

            try
            {
                TableInputFilter inputTable = new TableInputFilter
                {
                    Limit = Convert.ToInt32(Request.Form["pageLimits"]),
                    Index = Convert.ToInt32(Request.Form["pageTable"])
                };

                IEnumerable<Coin> result = IProxyServices.Eprocurement.Coin.GetAllCRUD(inputTable.Limit, inputTable.Index, out _countData, out _totalPages);
                summary = new JsonResultSummary<Coin>(result, inputTable.Limit, inputTable.Index, PagingCount, PagingTotal);
            }
            catch (Exception ex)
            {
                summary = new JsonResultSummary<Coin>(ex);
                Log.Error(ex, "Error Search Coin)");
            }
            return summary;
        }

        [HttpPost]
        public JsonReturn<Coin> Add()
        {
            Coin obj = null;
            JsonReturn<Coin> result = new JsonReturn<Coin>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hdnCode"]) && Convert.ToInt32(Request.Form["hdnCode"].ToString()) > 0)
                    obj = IProxyServices.Eprocurement.Coin.Get(Convert.ToInt32(Request.Form["hdnCode"]));

                if (obj == null) obj = new Coin();

                obj.Name = Request.Form["txtName"];
                obj.Initial = Request.Form["txtInitial"];

                if (obj.ID == 0)
                {
                    obj.RecordStatus = eStatus.Active;
                    obj.InsertDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Coin.Add(obj);
                }
                else
                {
                    obj.UpdateDate = DateTime.Now;
                    obj = IProxyServices.Eprocurement.Coin.Update(obj);
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error save Coin)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Coin> Load()
        {
            Coin obj = new Coin();
            JsonReturn<Coin> result = new JsonReturn<Coin>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]))
                {
                    obj = IProxyServices.Eprocurement.Coin.Get(Convert.ToInt32(Request.Form["id"]));
                }

                result.SetSuccess(obj);
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error Load Coin)");
            }
            return result;
        }

        [HttpPost]
        public JsonReturn<Coin> ChangeStatus()
        {
            Coin obj = null;
            JsonReturn<Coin> result = new JsonReturn<Coin>();

            try
            {
                if (!string.IsNullOrEmpty(Request.Form["id"]) && !string.IsNullOrEmpty(Request.Form["status"]))
                {
                    obj = IProxyServices.Eprocurement.Coin.Get(Convert.ToInt32(Request.Form["id"]));

                    if (obj != null)
                    {
                        obj.RecordStatus = (eStatus)Convert.ToInt32(Request.Form["status"]);
                        obj.UpdateDate = DateTime.Now;
                        obj = IProxyServices.Eprocurement.Coin.Update(obj);

                        result.SetSuccess(obj);
                    }
                    else result.SetNotFound(obj);
                }
            }
            catch (Exception ex)
            {
                result.SetException(ex, obj);
                Log.Error(ex, "Error ChangeStatus Coin)");
            }
            return result;
        }
    }
}
