﻿using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Proxy.Services;
using FS.Libraries.Security.CryptoSecurity.Services;
using FS.Libraries.Tools.SendMail.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FS.WebApplication.EprocurementWeb.Controllers
{
    public class ControllerBase : Controller
    {
        private readonly string sessionName = ConfigurationManager.AppSettings["SessionName"];

        private int _limit = 0;
        private int _index = 0;
        public int _countData = 0;
        public int _totalPages = 0;
        private Account _accountLogged;
        private Company _companyLogged;

        public Account UserSession { get; set; }

        public Account AccountLogged
        {
            get
            {
                _accountLogged = null;

                if (_accountLogged == null)
                {
                    _accountLogged = new Account();
                }

                _accountLogged.ID = 9999;

                return _accountLogged;
            }
            set => _accountLogged = value;
        }
        public Company CompanyLogged
        {
            get
            {
                _companyLogged = null;

                if (_companyLogged == null)
                {
                    _companyLogged = new Company();
                }

                _companyLogged.ID = 9999;

                return _companyLogged;
            }
            set => _companyLogged = value;
        }

        public int PagingLimit
        {
            get
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.Query["limit"]))
                    {
                        _limit = Convert.ToInt32(Request.Query["limit"]);
                    }
                }
                catch
                {
                    //--> Ignore
                }
                return _limit;
            }
            set => _limit = value;
        }

        public int PagingIndex
        {
            get
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.Query["index"]))
                    {
                        _index = Convert.ToInt32(Request.Query["index"]);
                    }
                }
                catch
                {
                    //--> Ignore
                }
                return _index;
            }
            set => _index = value;
        }

        public int PagingCount
        {
            get => _countData;
            set => _countData = value;
        }

        public int PagingTotal
        {
            get => _totalPages;
            set => _totalPages = value;
        }

        public string SessionName
        {
            get { return sessionName; }
        }

        
        public IConfiguration Configuration { get; set; }

        public IWebHostEnvironment Environment { get; set; }

        public EmailConfiguration EmailSettings
        {
            get
            {
                EmailConfiguration obj = null;
                try
                {
                    if (Configuration != null)
                    {
                        obj = Configuration.GetSection("EmailSettings").Get<EmailConfiguration>();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error get EmailSettings in appsettings.json");
                }
                return obj;
            }
        }

        private EprocurementContext _eprocurementContext { get; set; }

        private EmailConfiguration _emailConfiguration { get; set; }

        public ICryptoServices CryptoServices => new CryptoServices();

        public IProxyServices IProxyServices => new ProxyServices(_eprocurementContext, _emailConfiguration);

        public ControllerBase() { }

        public ControllerBase(EprocurementContext eprocurementContext)
        {
            _eprocurementContext = eprocurementContext;
        }

        public ControllerBase(EprocurementContext eprocurementContext, EmailConfiguration emailConfiguration)
        {
            _eprocurementContext = eprocurementContext;
            _emailConfiguration = emailConfiguration;
        }
    }
}
