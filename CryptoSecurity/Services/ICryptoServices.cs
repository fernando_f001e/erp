﻿using System;
using System.Collections.Generic;
using System.Text;
using FS.Libraries.Security.CryptoSecurity.RSA;

namespace FS.Libraries.Security.CryptoSecurity.Services
{
    public interface ICryptoServices
    {
        /// <summary>
        /// Computes the MD5 hash for the input data.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        string GetMD5(string input);

        /// <summary>
        /// Computes the SHA512 hash for the input data.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        string GetSHA512(string input);

        /// <summary>
        /// Encode with base64 the input data.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        string Base64Encode(string input);

        /// <summary>
        /// Decode with base64 the input data.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        string Base64Decode(string input);

        /// <summary>
        /// Return key`s with RSA
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> RSACreateKeys();

        /// <summary>
        /// Encrypt using RSA method
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="publicKey"></param>
        /// <returns></returns>
        string RSAEncrypt(string plainText, string publicKey);

        /// <summary>
        /// Encrypt using RSA method
        /// </summary>
        /// <returns>The ncrypt.</returns>
        /// <param name="plainText">Plain text.</param>
        /// <param name="publicKey">Public key.</param>
        string RSAEncrypt(string plainText, RSAParametersJson publicKey);

        /// <summary>
        /// Descrypt using RSA method
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        string RSADecrypt(string cipherText, string privateKey);

        /// <summary>
        /// Digital Signed data
        /// </summary>
        /// <param name="clearText"></param>
        /// <param name="privateKey"></param>
        /// <returns></returns>
        string RSASigned(string clearText, string privateKey);

        /// <summary>
        /// Descrypt using RSA method
        /// </summary>
        /// <returns>The igned.</returns>
        /// <param name="clearText">Clear text.</param>
        /// <param name="privateKey">Private key.</param>
        string RSASigned(string clearText, RSAParametersJson privateKey);

        /// <summary>
        /// Check if the digital signature is valid
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="signed"></param>
        /// <param name="privateKey"></param>
        /// <returns></returns>
        bool RSAIsValidSigned(string cipherText, string signed, string privateKey);

        /// <summary>
        /// Check if the digital RSA signature is valid
        /// </summary>
        /// <returns><c>true</c>, if s valid signed was RSAIed, <c>false</c> otherwise.</returns>
        /// <param name="cipherText">Cipher text.</param>
        /// <param name="signed">Signed.</param>
        /// <param name="privateKey">Private key.</param>
        bool RSAIsValidSigned(string cipherText, string signed, RSAParametersJson privateKey);

        /// <summary>
        /// Check cipher with RSA
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        bool RSAHasCipher(string cipherText);

        /// <summary>
        /// Obfuscate the clear string
        /// </summary>
        /// <param name="clearText"></param>
        /// <returns></returns>
        string Obfuscate(string clearText);


        /// <summary>
        /// Revert Obfuscate of string
        /// </summary>
        /// <param name="obfuscatedText"></param>
        /// <returns></returns>
        string Desobfuscate(string obfuscatedText);

        /// <summary>
        /// checks if a string was Obfuscate
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        bool HasObfuscate(string text);

        /// <summary>
        /// Generators the password.
        /// </summary>
        /// <returns>The password.</returns>
        /// <param name="tamanho">Tamanho.</param>
        string GeneratorPassword(int tamanho);
    }
}
