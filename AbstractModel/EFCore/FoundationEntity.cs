﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Libraries.Common.AbstractModel.EFCore
{
    /// <summary>
    /// Abstract Class FoundationEntity is base entity for all other entities
    /// </summary>
    public abstract class FoundationEntity
    {
        /// <summary>
        /// Status of register in database 
        /// </summary>  
        [Column("gestatus", Order = 98)]
        public int Status { get; set; }

        /// <summary>
        /// Date of inclusion or update of record in database
        /// </summary>       
        [DefaultValue(typeof(DateTime), "")]
        [Column("gedateupdate", Order = 99)]
        public DateTime? DateUpdate { get; set; }

        /// <summary>
        /// AccountId of the user who has added or updated the registry in database
        /// </summary>
        [Column("geaccountidupdate", Order = 100)]
        public int? AccountIDUpdate { get; set; }

        /// <summary>
        /// Account name of the user who has added or updated the registry
        /// </summary>
        [NotMapped]
        public string AccountUserNameUpdate { get; set; }

        /// <summary>
        /// Date of inclusion or update of record formated pt-BR("dd/MM/yyyy HH:mm") 
        /// </summary>
        [NotMapped]
        public string DateUpdateFormated
        {
            get
            {
                if (this.DateUpdate != null)
                {
                    return Convert.ToDateTime(this.DateUpdate).ToString("dd/MM/yyyy HH:mm");
                }
                else return string.Empty;
            }
        }
    }
}
