﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Common.AbstractModel.EFCore
{
    public class AbstractContext<TEntity> : IAbstractContext<TEntity> where TEntity : class, new()
    {
        /// <summary>
        /// Property _context is DbContext of TEntity current (Use Wisely)
        /// </summary>
        public DbContext _context { get; set; }
        /// <summary>
        /// Property DbSet with DbContext of TEntity current encapsulated
        /// </summary>
        public DbSet<TEntity> DbSet { get; set; }

        /// <summary>
        /// Constructor of class AbstractContext
        /// </summary>
        public AbstractContext() { }

        /// <summary>
        /// Constructor of class AbstractContext
        /// </summary>
        /// <param name="context"></param>
        public AbstractContext(DbContext context)
        {
            _context = context;
            DbSet = _context.Set<TEntity>();
        }


        /// <summary>
        /// Return all object
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        /// <summary>
        /// Returns objects by paging the data
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="index"></param>
        /// <param name="total"></param>
        /// <param name="totalPages"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll(int limit, int index, out int total, out int totalPages)
        {
            total = DbSet.Count();
            totalPages = (int)Math.Ceiling((double)total / limit);

            return DbSet.Skip((index - 1) * limit)
                        .Take(limit)
                        .ToList();
        }

        /// <summary>
        /// Returns objects by predicate and paging the data
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="limit"></param>
        /// <param name="index"></param>
        /// <param name="total"></param>
        /// <param name="totalPages"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll(int limit, int index, out int total, out int totalPages, Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate.ToString().Equals("f => False"))
            {
                total = DbSet.Count();
                totalPages = (int)Math.Ceiling((double)total / limit);

                return _context.Set<TEntity>()
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
            }

            total = DbSet.Where(predicate).Count();
            totalPages = (int)Math.Ceiling((double)total / limit);

            return _context.Set<TEntity>()
                    .Where(predicate)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        /// <summary>
        /// Returns objects by predicate and paging the data
        /// </summary>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.ToList();
        }

        /// <summary>
        /// Return objects by predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            if (includeProperties != null && includeProperties.Any())
            {
                foreach (var includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }

            return predicate.ToString().Equals("f => False") ? query : query.Where(predicate);
        }

        /// <summary>
        /// Return objects by predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return predicate.ToString().Equals("f => False") ? _context.Set<TEntity>() : _context.Set<TEntity>().Where(predicate);
        }
        
        public virtual IEnumerable<TEntity> GetAll(int limit, int index, out int total, out int totalPages, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            if (includeProperties != null && includeProperties.Length > 0)
            {
                foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }

            if (predicate != null && !predicate.ToString().Equals("f => False"))
            {
                query = query.Where(predicate);
            }

            total = query.Count();
            totalPages = limit > 0 && index > 0 ? (int)Math.Ceiling((double)total / limit) : 1;

            return limit > 0 && index > 0 ? query.Skip((index - 1) * limit).Take(limit).ToList() : query.ToList();
        }

        /// <summary>
        /// Return object by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity Get<TKey>(TKey id)
        {
            return DbSet.Find(id);
        }

        /// <summary>
        /// Return object by Predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual TEntity FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return predicate.ToString().Equals("f => False") ? _context.Set<TEntity>().FirstOrDefault() : _context.Set<TEntity>().FirstOrDefault(predicate);
        }

        /// <summary>
        /// Return object by Predicate and includeProperty
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public virtual TEntity FindBy(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return predicate.ToString().Equals("f => False") ? query.FirstOrDefault() :
                query.FirstOrDefault(predicate);
        }

        /// <summary>
        /// Return objects by predicate and includeProperties 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> FindLogBy(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return predicate.ToString().Equals("f => False") ? query.ToList() : query.Where(predicate).ToList();
        }

        /// <summary>
        /// Save data object in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TEntity Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            Save();
            return entity;
        }

        /// <summary>
        /// Update data object in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TEntity Update(TEntity entity)
        {
            _context.Entry<TEntity>(entity).State = EntityState.Modified;
            Save();
            return entity;
        }

        /// <summary>
        /// Remove object of database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Remove(TEntity entity)
        {
            _context.Entry<TEntity>(entity).State = EntityState.Deleted;
            _context.Remove(entity);
            Save();

            return true;
        }

        /// <summary>
        /// Remove object of database by predicate
        /// </summary>
        /// <param name="predicate"></param>
        public virtual bool RemoveWhere(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> entities = predicate.ToString().Equals("f => False") ? _context.Set<TEntity>() : _context.Set<TEntity>().Where(predicate);

            foreach (TEntity entity in entities)
            {
                _context.Entry<TEntity>(entity).State = EntityState.Deleted;
            }

            Save();

            return true;
        }

        private void Save()
        {
            _context.SaveChanges();
        }

       
        /// <summary>
        /// Save Log data object in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TEntity AddLog(TEntity entity, object previousObject, object newObject, eAdministrativeLogAction actionId)
        {
            _context.Set<TEntity>().Add(entity);

            _context.Entry<TEntity>(entity).Property("ActionDate").CurrentValue = DateTime.Now;
            _context.Entry<TEntity>(entity).Property("ActionID").CurrentValue = actionId;
            _context.Entry<TEntity>(entity).Property("TableName").CurrentValue = newObject.GetType().Name;

            if (previousObject != null)
            {
                _context.Entry<TEntity>(entity).Property("PreviousObject").CurrentValue = previousObject;
            }
            string newvalues = SerializeUtil.Serialize(newObject);
            string writeValues = SerializeUtil.Deserialize(newvalues);
            _context.Entry<TEntity>(entity).Property("NewObject").CurrentValue = writeValues;

            Save();

            return entity;
        }

        /// <summary>
        /// Save Log data object in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual List<TEntity> AddLog(List<TEntity> entities, object previousObject, object newObject, eAdministrativeLogAction actionId)
        {
            foreach (TEntity entity in entities)
            {
                _context.Entry<TEntity>(entity).Property("ActionDate").CurrentValue = DateTime.Now;
                _context.Entry<TEntity>(entity).Property("ActionID").CurrentValue = actionId;
                _context.Entry<TEntity>(entity).Property("TableName").CurrentValue = newObject.GetType().Name;
                if (previousObject != null)
                {
                    _context.Entry<TEntity>(entity).Property("PreviousObject").CurrentValue = previousObject;
                }
                string newvalues = SerializeUtil.Serialize(newObject);
                string writeValues = SerializeUtil.Deserialize(newvalues);
                _context.Entry<TEntity>(entity).Property("NewObject").CurrentValue = writeValues;
            }

            _context.AddRange(entities);

            Save();

            return entities;
        }

        /// <summary>
        /// Save Log data object in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public virtual List<TEntity> AddLog(List<TEntity> entities)
        {
            foreach (TEntity entity in entities)
            {
                _context.Entry<TEntity>(entity).Property("ActionDate").CurrentValue = DateTime.Now;
            }

            _context.AddRange(entities);

            Save();

            return entities;
        }
    }
}
