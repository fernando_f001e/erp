﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FS.Libraries.Common.AbstractModel.EFCore
{
    /// <summary>
    /// IAbstractContext interface with options CRUD by generic entity (repository)
    /// </summary>
    public interface IAbstractContext<TEntity>
    {
        /// <summary>
        /// Return all object
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Returns objects by paging the data
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="index"></param>
        /// <param name="total"></param>
        /// <param name="totalPages"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll(int limit, int index, out int total, out int totalPages);

        /// <summary>
        /// Returns objects by predicate and paging the data
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="index"></param>
        /// <param name="total"></param>
        /// <param name="totalPages"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll(int limit, int index, out int total, out int totalPages, Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Return objects by predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Return object by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity Get<TKey>(TKey id);

        /// <summary>
        /// Return object by Predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity FindBy(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Save data object in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Add(TEntity entity);

        /// <summary>
        /// Update data object in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Remove object of database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Remove(TEntity entity);
    }
}
