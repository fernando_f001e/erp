﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Model
{
    public class PurchaseOrderInputFilter
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public int ItemId { get; set; }
        public int RecodStatus { get; set; }
    }
}
