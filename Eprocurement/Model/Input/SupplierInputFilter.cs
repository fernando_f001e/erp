﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Model
{
    public class SupplierInputFilter
    {
        public string Name { get; set; }
        public string Cnpj { get; set; }
        public string Email { get; set; }
    }
}
