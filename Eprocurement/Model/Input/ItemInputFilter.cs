﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Model
{
    public class ItemInputFilter
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public int MeasuredUnitId { get; set; }
    }
}
