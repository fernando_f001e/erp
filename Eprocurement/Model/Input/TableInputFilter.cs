﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Model
{
    public class TableInputFilter
    {
        public int Index { get; set; }
        public int Limit { get; set; }
    }
}
