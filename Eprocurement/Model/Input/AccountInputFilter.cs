﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Model
{
    public class AccountInputFilter
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int? Type { get; set; }
    }
}
