﻿using FS.Libraries.Common.Helpers;
using System;

namespace FS.Libraries.Eprocurement.Model
{
    public class PurchaseOrderViewModel
    {
        public int ID { get; set; }
        public int CompanyID { get; set; }
        public int AccountID { get; set; }
        public int ItemID { get; set; }
        public string Item { get; set; }
        public int MeasuredUnitID { get; set; }
        public string MeasuredUnit { get; set; }
        public decimal Qtde { get; set; }
        public int Factor { get; set; }
        public int QuotationID { get; set; }
        public eStatus RecordStatus { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
