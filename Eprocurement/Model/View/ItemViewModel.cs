﻿using FS.Libraries.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Model
{
    public class ItemViewModel
    {
        public int ID { get; set; }

        public int CompanyID { get; set; }
        
        public string Name { get; set; }
        
        public int MeasuredUnitID { get; set; }

        public string MeasuredUnit { get; set; }

        public eStatus RecordStatus { get; set; }
        
        public DateTime InsertDate { get; set; }
        
        public DateTime? UpdateDate { get; set; }
    }
}
