﻿using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Data.InitialData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Context
{
    public class EprocurementContext : DbContext
    {
        public DbSet<Account> Account { get; set; }
        public DbSet<AdministrativeLog> AdministrativeLog { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<PaymentCondition> PaymentCondition { get; set; }
        public DbSet<Freight> Freight { get; set; }
        public DbSet<Coin> Coin { get; set; }
        public DbSet<MeasuredUnit> MeasuredUnit { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<Supplier> Supplier { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public DbSet<Quotation> Quotation { get; set; }

        public EprocurementContext() { }
        public EprocurementContext(DbContextOptions<EprocurementContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.UseIdentityColumns();

            //--> Start data in database
            modelBuilder.Entity<Account>().HasData(InitialDataAccount.Account);
            modelBuilder.Entity<Company>().HasData(InitialDataCompany.Company);
            modelBuilder.Entity<Country>().HasData(InitialDataGeoCountry.Country);
            modelBuilder.Entity<Region>().HasData(InitialDataGeoRegion.Region);
            modelBuilder.Entity<City>().HasData(InitialDataGeoCity.City);
            modelBuilder.Entity<State>().HasData(InitialDataGeoState.State);

        }
    }
}
