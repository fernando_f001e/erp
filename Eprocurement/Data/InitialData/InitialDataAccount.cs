﻿using System;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data.InitialData
{
    public class InitialDataAccount
    {
        public static Account[] Account = new Account[]
        {
            new Account
            {
                ID = 9999,
                Name = "Fernando de Mello Sanchez",
                Email = "fernando_f001e@hotmail.com",
                Password = "85FC2A6C376D74F3AB34BBB4E500502FE8C26CDBE3598B6553804FB41011D596A44D9C3B7AB8FBD1DA4CFD2EE5AE14044C79CFC07B243723B048181C6627A6F7", //Zeon3539!
                Type = 1,
                RecordStatus = (int)eStatus.Active,
                InsertDate = DateTime.Now
            }
        };
    }
}
