﻿using System;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data.InitialData
{
    public class InitialDataCompany
    {
        public static Company[] Company = new Company[]
        {
            new Company
            {
                ID = 9999,
                Name = "Fernando de Mello Sanchez ME",
                CNPJ = "28.270.131/0001-08",
                RecordStatus = eStatus.Active,
                InsertDate = DateTime.Now
            }
        };
    }
}
