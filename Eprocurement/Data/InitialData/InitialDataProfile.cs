﻿using System;
using Zeon.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data.InitialData
{
    public class InitialDataProfile
    {
        public static Profile[] AddMenu = {
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU SINISTROS",
                Role = "MENU_SINISTER",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU DISTRIBUIÇÃO",
                Role = "MENU_DISTRIBUTION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU IMPORTAÇÕES",
                Role = "MENU_IMPORTS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU FAROL",
                Role = "MENU_LIGHTHOUSE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU RELATÓRIOS",
                Role = "MENU_REPORTS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU MONITORIA",
                Role = "MENU_MONITORING",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU ADMINISTRAÇÃO",
                Role = "MENU_ADMINISTRATION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty

            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "MENU CONFIGURAÇÕES",
                Role = "MENU_CONFIGURATION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            }
        };

        public static Profile[] AddItemsAdministration = {
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "ADM - CONTAS",
                Role = "RW_ADM_ACCOUNTS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "ADM - GRUPOS",
                Role = "RW_ADM_GROUPS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "ADM - PERMISSÕES",
                Role = "RW_ADM_PERMISSIONS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "ADM - LOGS",
                Role = "RW_ADM_LOGS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
        };

        public static Profile[] AddItemsConfiguration = {
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - AREA",
                Role = "RW_CFG_AREA",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - BANCOS",
                Role = "RW_CFG_BANK",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - FERIADOS",
                Role = "RW_CFG_HOLIDAY",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - FILIAIS",
                Role = "RW_CFG_BRANCH",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - SEGURADORAS",
                Role = "RW_CFG_INSURERS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - TIPO DE NEGÓCIO",
                Role = "RW_CFG_BUSINESSTYPE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - SEGURADO",
                Role = "RW_CFG_INSURED",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - REGULADOR",
                Role = "RW_CFG_EXPERT",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - REGIÕES DE ATENDIMENTO",
                Role = "RW_CFG_SERVICEREGIONS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - ESTÁGIOS",
                Role = "RW_CFG_STAGES",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - TIPO DE VISTORIA",
                Role = "RW_CFG_REGULATIONTYPE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - TIPO DE DOCUMENTOS",
                Role = "RW_CFG_DOCUMENTTYPE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - TIPO DE IMAGENS",
                Role = "RW_CFG_IMAGESTTYPE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - SISTEMA DE ORÇAMENTAÇÃO",
                Role = "RW_CFG_BUDGETSYSTEM",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - FORMA DE VISTORIA",
                Role = "RW_CFG_SCHEDULINGTYPE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - AGENDADO COM (CONTATO)",
                Role = "RW_CFG_SCHEDULINGCONTACT",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - OFICINA",
                Role = "RW_CFG_WORKSHOP",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - TIPO DE VEÍCULOS",
                Role = "RW_CFG_AUTOTYPE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "CONF - MODELOS DE VEÍCULOS",
                Role = "RW_CFG_AUTOBRAND",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            }
        };


        public static Profile[] AddItemsSinister = {
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA PESQUISA",
                Role = "RW_REG_TABSEARCH",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA DETALHES (LEITURA)",
                Role = "RD_REG_TABDETAILS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA DETALHES (ESCRITA)",
                Role = "RW_REG_TABDETAILS",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA AGENDAMENTOS (LEITURA)",
                Role = "RD_REG_TABSCHEDULING",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA AGENDAMENTOS (ESCRITA)",
                Role = "RW_REG_TABSCHEDULING",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA RETORNO OFICINA (LEITURA)",
                Role = "RD_REG_TABWORKSHOP",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA RETORNO OFICINA (ESCRITA)",
                Role = "RW_REG_TABWORKSHOP",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA REGULAÇÃO (LEITURA)",
                Role = "RD_REG_TABREGULATION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA REGULAÇÃO (ESCRITA)",
                Role = "RW_REG_TABREGULATION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA ANÁLISE (LEITURA)",
                Role = "RD_REG_TABANALYZE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA ANÁLISE (ESCRITA)",
                Role = "RW_REG_TABANALYZE",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA DIGITAÇÃO (LEITURA)",
                Role = "RD_REG_TABDIGITATION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
            new Profile
            {
                ID = Guid.NewGuid(),
                Name = "REG - ABA DIGITAÇÃO (ESCRITA)",
                Role = "RW_REG_TABDIGITATION",
                Status = Convert.ToBoolean(eStatus.Active),
                DateUpdate = null,
                AccountIDUpdate = Guid.Empty
            },
        };
    }
}
