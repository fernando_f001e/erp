﻿
namespace FS.Libraries.Eprocurement.Data.InitialData
{
    public class InitialDataGeoRegion
    {
        private static int BRASIL = 1;

        public static Region[] Region = new Region[]
        {
            new Region
            {
                ID = 1,
                CountryID = BRASIL,
                Name= "NORTE"
            },
            new Region
            {
                ID = 2,
                CountryID = BRASIL,
                Name= "NORDESTE"
            },
            new Region
            {
                ID = 3,
                CountryID = BRASIL,
                Name= "CENTRO-OESTE"
            },
            new Region
            {
                ID = 4,
                CountryID = BRASIL,
                Name= "SUL"
            },
            new Region
            {
                ID = 5,
                CountryID = BRASIL,
                Name= "SUDESTE"
            }
        };
    }
}
