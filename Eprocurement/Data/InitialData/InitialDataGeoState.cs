﻿using System;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data.InitialData
{
    /// <summary>
    /// Initial data geo state.
    /// </summary>
    public class InitialDataGeoState
    {
        private static int countryID = 1;

        private static int NORTE = 1;
        private static int NORDESTE = 2;
        private static int CENTRO_OESTE = 3;
        private static int SUL = 4;
        private static int SUDESTE = 5;

        /// <summary>
        /// Add state.
        /// </summary>
        public static State[] State = new State[]
        {
            new State { ID = 1, CountryID = countryID, RegionID = NORTE, Name = "ACRE", Abbreviature = "AC"},
            new State { ID = 2, CountryID = countryID, RegionID = NORDESTE, Name = "ALAGOAS", Abbreviature = "AL"},
            new State { ID = 3, CountryID = countryID, RegionID = NORTE, Name = "AMAPÁ", Abbreviature = "AP"},
            new State { ID = 4, CountryID = countryID, RegionID = NORTE, Name = "AMAZONAS", Abbreviature = "AM"},
            new State { ID = 5, CountryID = countryID, RegionID = NORDESTE, Name = "BAHIA", Abbreviature = "BA"},
            new State { ID = 6, CountryID = countryID, RegionID = NORDESTE, Name = "CEARÁ", Abbreviature = "CE"},
            new State { ID = 7, CountryID = countryID, RegionID = CENTRO_OESTE, Name = "DISTRITO FEDERAL", Abbreviature = "DF"},
            new State { ID = 8, CountryID = countryID, RegionID = SUDESTE, Name = "ESPÍRITO SANTO", Abbreviature = "ES"},
            new State { ID = 9, CountryID = countryID, RegionID = CENTRO_OESTE, Name = "GOIÁS", Abbreviature = "GO"},
            new State { ID = 10, CountryID = countryID, RegionID = NORDESTE, Name = "MARANHÃO", Abbreviature = "MA"},
            new State { ID = 11, CountryID = countryID, RegionID = CENTRO_OESTE, Name = "MATO GROSSO", Abbreviature = "MT"},
            new State { ID = 12, CountryID = countryID, RegionID = CENTRO_OESTE, Name = "MATO GROSSO DO SUL", Abbreviature = "MS"},
            new State { ID = 13, CountryID = countryID, RegionID = SUDESTE, Name = "MINAS GERAIS", Abbreviature = "MG"},
            new State { ID = 14, CountryID = countryID, RegionID = NORTE, Name = "PARÁ", Abbreviature = "PA"},
            new State { ID = 15, CountryID = countryID, RegionID = NORDESTE, Name = "PARAÍBA", Abbreviature = "PB"},
            new State { ID = 16, CountryID = countryID, RegionID = SUL, Name = "PARANÁ", Abbreviature = "PR"},
            new State { ID = 17, CountryID = countryID, RegionID = NORDESTE, Name = "PERNAMBUCO", Abbreviature = "PE"},
            new State { ID = 18, CountryID = countryID, RegionID = NORDESTE, Name = "PIAUÍ", Abbreviature = "PI"},
            new State { ID = 19, CountryID = countryID, RegionID = SUDESTE, Name = "RIO DE JANEIRO", Abbreviature = "RJ"},
            new State { ID = 20, CountryID = countryID, RegionID = NORDESTE, Name = "RIO GRANDE DO NORTE", Abbreviature = "RN"},
            new State { ID = 21, CountryID = countryID, RegionID = SUL, Name = "RIO GRANDE DO SUL", Abbreviature = "RS"},
            new State { ID = 22, CountryID = countryID, RegionID = NORTE, Name = "RONDÔNIA", Abbreviature = "RO"},
            new State { ID = 23, CountryID = countryID, RegionID = NORTE, Name = "RORAÍMA", Abbreviature = "RR"},
            new State { ID = 24, CountryID = countryID, RegionID = SUL, Name = "SANTA CATARINA", Abbreviature = "SC"},
            new State { ID = 25, CountryID = countryID, RegionID = SUDESTE, Name = "SÃO PAULO", Abbreviature = "SP"},
            new State { ID = 26, CountryID = countryID, RegionID = NORDESTE, Name = "SERGIPE", Abbreviature = "SE"},
            new State { ID = 27, CountryID = countryID, RegionID = NORTE, Name = "TOCANTINS", Abbreviature = "TO"}
        };
    }
}
