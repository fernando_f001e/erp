﻿using System;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data.InitialData
{
    public class InitialDataGeoCountry
    {
        public static Country[] Country = new Country[]
        {
            new Country
            {
                ID = 1,
                Name = "BRASIL",
                Language = "pt-BR"
            }
        };
    }
}
