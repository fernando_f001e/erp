﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("region")]
    public class Region
    {   
        [Key, Column("regionid")]
        public int ID { get; set; }

        [Column("countryid")]
        public int CountryID { get; set; }
        public Country Country { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        #region Contructor
        public Region() { }

        public Region(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
