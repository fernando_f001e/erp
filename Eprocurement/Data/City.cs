﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("city")]
    public class City
    {
        [Key, Column("cityid")]
        public int ID { get; set; }

        [Column("stateid")]
        public int StateID { get; set; }
        public State State { get; set; }

        [Column("name")]
        public  string Name { get; set; }

        #region Contructor
        public City() { }

        public City(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
