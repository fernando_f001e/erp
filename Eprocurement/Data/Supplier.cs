﻿using FS.Libraries.Common.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("supplier")]
    public class Supplier
    {
        [Key, Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("cnpj")]
        public string Cnpj { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("recordstatus")]
        public eStatus RecordStatus { get; set; }

        [Column("insertdate")]
        public DateTime InsertDate { get; set; }

        [Column("updatedate")]
        public DateTime? UpdateDate { get; set; }

        #region Contructor
        public Supplier() { }

        public Supplier(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
