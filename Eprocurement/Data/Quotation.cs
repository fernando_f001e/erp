﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("quotation")]
    public class Quotation
    {
        [Key, Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column("companyid")]
        public int CompanyID { get; set; }
        public Company Company { get; set; }

        [Column("accountid")]
        public int AccountID { get; set; }
        public Account Account { get; set; }

        [Column("recordstatus")]
        public eStatus RecordStatus { get; set; }

        [Column("insertdate")]
        public DateTime InsertDate { get; set; }

        [Column("updatedate")]
        public DateTime? UpdateDate { get; set; }

        #region Contructor
        public Quotation() { }

        public Quotation(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
