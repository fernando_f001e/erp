﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("purchaseorder")]
    public class PurchaseOrder
    {
        [Key, Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column("companyid")]
        public int CompanyID { get; set; }
        public Company Company { get; set; }

        [Column("accountid")]
        public int AccountID { get; set; }
        public Account Account { get; set; }

        [Column("itemid")]
        public int ItemID { get; set; }
        public Item Item { get; set; }

        [Column("measuredunitid")]
        public int MeasuredUnitID { get; set; }
        public MeasuredUnit MeasuredUnit { get; set; }

        [Column("qtde")]
        public decimal Qtde { get; set; }

        [Column("factor")]
        public int Factor { get; set; }

        [Column("quotationid")]
        public int? QuotationID { get; set; }

        public Quotation Quotation { get; set; }

        [Column("recordstatus")]
        public eStatus RecordStatus { get; set; }

        [Column("insertdate")]
        public DateTime InsertDate { get; set; }

        [Column("updatedate")]
        public DateTime? UpdateDate { get; set; }

        #region Contructor
        public PurchaseOrder() { }

        public PurchaseOrder(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
