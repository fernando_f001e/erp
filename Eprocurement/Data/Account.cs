﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Libraries.Eprocurement.Data
{ 
    [Table("account")]
    public class Account
    {
        [Key, Column("accountid")]
        public int ID { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("email")]
        public string Email { get; set; }
       
        [Column("password")]
        public string Password { get; set; }
        
        [Column("type")]
        public int Type { get; set; }
        
        [Column("lastaccess")]
        public DateTime? LastAccess { get; set; }

        [Column("recordstatus")]
        public int RecordStatus { get; set; }

        [Column("insertdate")]
        public DateTime InsertDate { get; set; }

        [Column("updatedate")]
        public DateTime? UpdateDate { get; set; }

        #region Contructor
        public Account() { }

        public Account(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
