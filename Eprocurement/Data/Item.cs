﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("item")]
    public class Item
    {
        [Key, Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column("companyId")]
        public int CompanyID { get; set; }

        public Company Company { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("measuredunitid")]
        public int MeasuredUnitID { get; set; }

        public MeasuredUnit MeasuredUnit { get; set; }

        [Column("recordstatus")]
        public eStatus RecordStatus { get; set; }

        [Column("insertdate")]
        public DateTime InsertDate { get; set; }

        [Column("updatedate")]
        public DateTime? UpdateDate { get; set; }

        #region Contructor
        public Item() { }

        public Item(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
