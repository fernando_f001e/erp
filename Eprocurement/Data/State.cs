﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("state")]
    public class State
    { 
        [Key, Column("stateid")]
        public int ID { get; set; }

        [Column("countryid")]
        public int CountryID { get; set; }
        public Country Country { get; set; }
        
        [Column("regionid")]
        public int RegionID { get; set; }
        public Region Region { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("abbreviature")]
        public string Abbreviature { get; set; }

        #region Contructor
        public State() { }

        public State(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
