﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("administrativelog")]
    public class AdministrativeLog
    {
        [Key, Column("administrativelogid")]
        public int ID { get; set; }

        [Column("companyid")]
        public int CompanyID { get; set; }
        public Company Company { get; set; }

        [Column("accountid")]
        public int AccountID { get; set; }
        public Account Account { get; set; }
        
        [Column("actiondate")]
        public DateTime? ActionDate { get; set; }
        
        [Column("actionid")]
        public eAdministrativeLogAction ActionID { get; set; }
        
        [Column("tablename")]
        public string TableName { get; set; }
        
        [Column("previousobject")]
        public string PreviousObject { get; set; }
        
        [Column("newobject")]
        public string NewObject { get; set; }

        #region Contructor
    
        public AdministrativeLog() { }

        public AdministrativeLog(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
