﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FS.Libraries.Common.Helpers;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("freight")]
    public class Freight
    {
        [Key, Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("recordstatus")]
        public eStatus RecordStatus { get; set; }

        [Column("insertdate")]
        public DateTime InsertDate { get; set; }

        [Column("updatedate")]
        public DateTime? UpdateDate { get; set; }

        #region Contructor
        public Freight() { }

        public Freight(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
