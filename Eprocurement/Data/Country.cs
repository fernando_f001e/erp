﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FS.Libraries.Common.AbstractModel.EFCore;

namespace FS.Libraries.Eprocurement.Data
{
    [Table("country")]
    public class Country
    {
        [Key, Column("countryid")]
        public int ID { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("language")]
        public string Language { get; set; }

        #region Contructor
        public Country() { }

        public Country(int id)
        {
            this.ID = id;
        }
        #endregion
    }
}
