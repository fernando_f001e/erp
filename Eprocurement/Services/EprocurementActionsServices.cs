﻿using FS.Libraries.Eprocurement.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Services
{
    public class EprocurementActionsServices : EprocurementCRUDServices, IEprocurementServices
    {
        private EprocurementContext _context = null;

        public EprocurementActionsServices(EprocurementContext context) : base(context)
        {
            _context = context;
        }
    }
}
