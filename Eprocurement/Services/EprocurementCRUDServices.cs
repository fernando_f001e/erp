﻿using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.CRUD;
using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Services
{
    public class EprocurementCRUDServices
    {
        private EprocurementContext _context = null;

        public EprocurementCRUDServices(EprocurementContext context)
        {
            _context = context;
        }

        public CRUDAccount Account=> new CRUDAccount(_context);
        public CRUDAdministrativeLog AdministrativeLog => new CRUDAdministrativeLog(_context);
        public CRUDCity City => new CRUDCity(_context);
        public CRUDCompany Company => new CRUDCompany(_context);
        public CRUDCountry Country => new CRUDCountry(_context);
        public CRUDRegion Region => new CRUDRegion(_context);
        public CRUDState State => new CRUDState(_context);
        public CRUDPaymentCondition PaymentCondition => new CRUDPaymentCondition(_context);
        public CRUDFreight Freight => new CRUDFreight(_context);
        public CRUDCoin Coin => new CRUDCoin(_context);
        public CRUDMeasuredUnit MeasuredUnit => new CRUDMeasuredUnit(_context);
        public CRUDItem Item => new CRUDItem(_context);
        public CRUDSupplier Supplier => new CRUDSupplier(_context);
        public CRUDPurchaseOrder PurchaseOrder => new CRUDPurchaseOrder(_context);
        public CRUDQuotation Quotation => new CRUDQuotation(_context);

    }
}
