﻿using FS.Libraries.Eprocurement.CRUD;
using System;
using System.Collections.Generic;
using System.Text;

namespace FS.Libraries.Eprocurement.Services
{
    public interface IEprocurementServices
    {
        CRUDAccount Account{ get; }
        CRUDAdministrativeLog AdministrativeLog { get; }
        CRUDCity City { get; }
        CRUDCompany Company { get; }
        CRUDCountry Country { get; }
        CRUDRegion Region { get; }
        CRUDState State { get; }
        CRUDPaymentCondition PaymentCondition { get; }
        CRUDFreight Freight { get; }
        CRUDCoin Coin { get; }
        CRUDMeasuredUnit MeasuredUnit { get; }
        CRUDItem Item { get; }
        CRUDSupplier Supplier { get; }
        CRUDPurchaseOrder PurchaseOrder { get; }
        CRUDQuotation Quotation { get; }
    }
}
