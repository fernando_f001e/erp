﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDItem : AbstractContext<Item>
    {
        internal CRUDItem(EprocurementContext context) : base(context) { }

        public IEnumerable<ItemViewModel> GetAllCRUD(ItemInputFilter input, int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD(input);

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<ItemViewModel> GetAllCRUD(ItemInputFilter input)
        {
            var query = from a in _context.Set<Item>()
                        .Where(t => t.CompanyID == input.CompanyId)
                        .Where(t => string.IsNullOrEmpty(input.Name) || t.Name.Contains(input.Name))
                        .Where(t => input.MeasuredUnitId == 0 || t.MeasuredUnitID == input.MeasuredUnitId)
                        
                        join b in _context.Set<MeasuredUnit>()
                        on a.MeasuredUnitID equals b.ID into measuredUnit
                        from b in measuredUnit.DefaultIfEmpty()

                        select new ItemViewModel()
                        {
                            ID = a.ID,
                            CompanyID = a.CompanyID,
                            Name = a.Name,
                            MeasuredUnitID = a.MeasuredUnitID,
                            MeasuredUnit = b.Initial,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
