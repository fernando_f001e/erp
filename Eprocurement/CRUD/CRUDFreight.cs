﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDFreight : AbstractContext<Freight>
    {
        internal CRUDFreight(EprocurementContext context) : base(context) { }

        public IEnumerable<Freight> GetAllCRUD(int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD();

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<Freight> GetAllCRUD()
        {
            var query = from a in _context.Set<Freight>()
                      
                        select new Freight()
                        {
                            ID = a.ID,
                            Name = a.Name,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
