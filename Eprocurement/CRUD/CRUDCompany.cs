﻿using System;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    /// <summary>
    /// Implements actions the <see cref="Company"/> class
    /// </summary>
    public class CRUDCompany : AbstractContext<Company>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Foundation.Core.CRUD.CRUDCompany"/> class.
        /// </summary>
        /// <param name="context">Context.</param>
        internal CRUDCompany(EprocurementContext context) : base(context) { }
    }
}
