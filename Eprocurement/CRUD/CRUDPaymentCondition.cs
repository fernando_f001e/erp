﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDPaymentCondition : AbstractContext<PaymentCondition>
    {
        internal CRUDPaymentCondition(EprocurementContext context) : base(context) { }

        public IEnumerable<PaymentCondition> GetAllCRUD(int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD();

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<PaymentCondition> GetAllCRUD()
        {
            var query = from a in _context.Set<PaymentCondition>()
                        
                        select new PaymentCondition()
                        {
                            ID = a.ID,
                            Description = a.Description,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
