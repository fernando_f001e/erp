﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDMeasuredUnit : AbstractContext<MeasuredUnit>
    {
        internal CRUDMeasuredUnit(EprocurementContext context) : base(context) { }

        public IEnumerable<MeasuredUnit> GetAllCRUD(int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD();

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<MeasuredUnit> GetAllCRUD()
        {
            var query = from a in _context.Set<MeasuredUnit>()

                        select new MeasuredUnit()
                        {
                            ID = a.ID,
                            Name = a.Name,
                            Initial = a.Initial,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
