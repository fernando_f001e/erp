﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDCoin : AbstractContext<Coin>
    {
        internal CRUDCoin(EprocurementContext context) : base(context) { }

        public IEnumerable<Coin> GetAllCRUD(int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD();

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<Coin> GetAllCRUD()
        {
            var query = from a in _context.Set<Coin>()

                        select new Coin()
                        {
                            ID = a.ID,
                            Name = a.Name,
                            Initial = a.Initial,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
