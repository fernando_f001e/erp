﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDQuotation : AbstractContext<Quotation>
    {
        internal CRUDQuotation(EprocurementContext context) : base(context) { }
    }
}
