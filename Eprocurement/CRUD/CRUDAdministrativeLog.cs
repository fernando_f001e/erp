﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    /// <summary>
    /// Class CRUDAdministrativeLog that implements AdministrativeLog actions
    /// </summary>
    public class CRUDAdministrativeLog : AbstractContext<AdministrativeLog>
    {
        internal CRUDAdministrativeLog(EprocurementContext context) : base(context) { }

        public IQueryable<AdministrativeLog> GetAll(int companyId, string name)
        {
            var query = from t in DbSet.Where(t => t.CompanyID == companyId)
                        join a in _context.Set<Account>() on t.AccountID equals a.ID
                            into Account
                        select new AdministrativeLog()
                        {
                            ID = t.ID,
                            CompanyID = t.CompanyID,
                            AccountID = t.AccountID,
                            ActionDate = t.ActionDate,
                            ActionID = t.ActionID,
                            TableName = t.TableName,
                            PreviousObject = t.PreviousObject,
                            NewObject = t.NewObject
                        };

            return query.OrderBy(s => s.ActionDate);

        }

        public IQueryable<AdministrativeLog> GetAll(int companyId)
        {
            var query = from t in DbSet.Where(t => t.CompanyID == companyId)
                        join a in _context.Set<Account>() on t.AccountID equals a.ID
                            into Account
                        select new AdministrativeLog()
                        {
                            ID = t.ID,
                            CompanyID = t.CompanyID,
                            AccountID = t.AccountID,
                            ActionDate = t.ActionDate,
                            ActionID = t.ActionID,
                            TableName = t.TableName,
                            PreviousObject = t.PreviousObject,
                            NewObject = t.NewObject
                        };

            return query.OrderBy(s => s.ActionDate);

        }

        public IEnumerable<AdministrativeLog> GetAll(int companyId, int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAll(companyId);

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ActionDate)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IEnumerable<AdministrativeLog> GetAll(int companyId, int limit, int index, out int total, out int totalPage, string name)
        {
            var query = this.GetAll(companyId, name);

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ActionDate)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

    }
}
