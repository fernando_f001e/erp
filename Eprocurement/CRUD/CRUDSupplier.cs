﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDSupplier : AbstractContext<Supplier>
    {
        internal CRUDSupplier(EprocurementContext context) : base(context) { }

        public IEnumerable<Supplier> GetAllCRUD(SupplierInputFilter input, int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD(input);

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<Supplier> GetAllCRUD(SupplierInputFilter input)
        {
            var query = from a in _context.Set<Supplier>()
                        .Where(t => string.IsNullOrEmpty(input.Cnpj) || t.Cnpj.Contains(input.Cnpj))
                        .Where(t => string.IsNullOrEmpty(input.Name) || t.Name.Contains(input.Name))
                        select new Supplier()
                        {
                            ID = a.ID,
                            Name = a.Name,
                            Cnpj = a.Cnpj,
                            Email = a.Email,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
