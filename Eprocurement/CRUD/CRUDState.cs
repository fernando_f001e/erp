﻿using System;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    /// <summary>
    /// Implements actions the <see cref="State"/> class
    /// </summary>
    public class CRUDState : AbstractContext<State>
    {
        internal CRUDState(EprocurementContext context) : base(context) { }
    }
}
