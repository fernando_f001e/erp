﻿using System;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    /// <summary>
    /// Implements actions the <see cref="City"/> class
    /// </summary>
    public class CRUDCity : AbstractContext<City>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Foundation.Core.CRUD.CRUDCity"/> class.
        /// </summary>
        /// <param name="context">Context.</param>
        internal CRUDCity(EprocurementContext context) : base(context) { }
    }
}
