﻿using System;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;

namespace FS.Libraries.Eprocurement.CRUD
{
    /// <summary>
    /// Implements actions the <see cref="Country"/> class
    /// </summary>
    public class CRUDCountry : AbstractContext<Country>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Foundation.Core.CRUD.CRUDCountry"/> class.
        /// </summary>
        /// <param name="context">Context.</param>
        internal CRUDCountry(EprocurementContext context) : base(context) { }
    }
}
