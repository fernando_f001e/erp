﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDPurchaseOrder : AbstractContext<PurchaseOrder>
    {
        internal CRUDPurchaseOrder(EprocurementContext context) : base(context) { }

        public IEnumerable<PurchaseOrderViewModel> GetAllCRUD(PurchaseOrderInputFilter input, int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD(input);

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<PurchaseOrderViewModel> GetAllCRUD(PurchaseOrderInputFilter input)
        {
            var query = from a in _context.Set<PurchaseOrder>()
                        .Where(t => t.CompanyID == input.CompanyId)
                        .Where(t => t.AccountID == input.AccountId)
                        .Where(t => input.Id == 0 || t.ID == input.Id)
                        .Where(t => string.IsNullOrEmpty(input.Name) || t.Item.Name.Contains(input.Name))
                        //.Where(t => input.RecodStatus == 0 && (input.RecodStatus).Equals(t.RecordStatus))

                        join b in _context.Set<MeasuredUnit>()
                        on a.MeasuredUnitID equals b.ID into measuredUnit
                        from b in measuredUnit.DefaultIfEmpty()

                        join c in _context.Set<Item>()
                        on a.ItemID equals c.ID into item
                        from c in item.DefaultIfEmpty()

                        join d in _context.Set<Quotation>()
                        on a.QuotationID equals d.ID into quotation
                        from d in quotation.DefaultIfEmpty()

                        select new PurchaseOrderViewModel()
                        {
                            ID = a.ID,
                            AccountID = a.AccountID,
                            CompanyID = a.CompanyID,
                            MeasuredUnitID = a.MeasuredUnitID,
                            MeasuredUnit = b.Initial,
                            ItemID = a.ItemID,
                            Item = c.Name,
                            Qtde = a.Qtde,
                            Factor = a.Factor,
                            QuotationID = d.ID,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
