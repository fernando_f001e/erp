﻿using System;
using System.Collections.Generic;
using System.Linq;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;
using FS.Libraries.Eprocurement.Model;

namespace FS.Libraries.Eprocurement.CRUD
{
    public class CRUDAccount : AbstractContext<Account>
    {
        internal CRUDAccount(EprocurementContext context) : base(context) { }

        public IEnumerable<Account> GetAllCRUD(AccountInputFilter input, int limit, int index, out int total, out int totalPage)
        {
            var query = this.GetAllCRUD(input);

            total = query.Count();
            totalPage = (int)Math.Ceiling((double)total / limit);

            return query
                    .OrderBy(s => s.ID)
                    .Skip((index - 1) * limit)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<Account> GetAllCRUD(AccountInputFilter input)
        {
            var query = from a in _context.Set<Account>()
                        .Where(t => string.IsNullOrEmpty(input.Name) || t.Name.Contains(input.Name))
                        .Where(t => string.IsNullOrEmpty(input.Email) || t.Email.Contains(input.Email))
                        .Where(t => input.Type == 0 || t.Type == input.Type)
                        select new Account()
                        {
                            ID = a.ID,
                            Name = a.Name,
                            Email = a.Email,
                            Password = a.Password,
                            Type = a.Type,
                            LastAccess = a.LastAccess,
                            RecordStatus = a.RecordStatus,
                            InsertDate = a.InsertDate,
                            UpdateDate = a.UpdateDate
                        };

            return query.OrderBy(s => s.ID);
        }
    }
}
