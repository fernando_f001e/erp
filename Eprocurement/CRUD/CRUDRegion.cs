﻿using System;
using FS.Libraries.Common.AbstractModel.EFCore;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Data;


namespace FS.Libraries.Eprocurement.CRUD
{
    /// <summary>
    /// Implements actions the <see cref="Region"/> class
    /// </summary>
    public class CRUDRegion : AbstractContext<Region>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Zeon.Foundation.Core.CRUD.CRUDRegion"/> class.
        /// </summary>
        /// <param name="context">Context.</param>
        internal CRUDRegion(EprocurementContext context) : base(context) { }
    }
}
