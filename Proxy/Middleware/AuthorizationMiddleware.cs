﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FS.Libraries.Common.Helpers;
using FS.Libraries.Security.CryptoSecurity.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using FS.Libraries.Security.CryptoSecurity.RSA;
using FS.Libraries.Proxy.Services;

namespace FS.Libraries.Proxy.Middleware
{
    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<AuthorizationMiddleware> _logger;

        IConfiguration Configuration { get; set; }
        IProxyServices proxyServices = new ProxyServices();

        /// <summary>
        /// Gets the crypto services.
        /// </summary>
        /// <value>The crypto services.</value>
        public ICryptoServices CryptoServices => new CryptoServices();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:IAP.Proxy.Middleware.AuthorizationMiddleware"/> class.
        /// </summary>
        /// <param name="next">Next.</param>
        /// <param name="logger">Logger.</param>
        public AuthorizationMiddleware(RequestDelegate next, ILogger<AuthorizationMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// Invoke the specified context and configuration.
        /// </summary>
        /// <returns>The invoke.</returns>
        /// <param name="context">Context.</param>
        /// <param name="configuration">Configuration.</param>
        public async Task Invoke(HttpContext context, IConfiguration configuration)
        {
            Contract.Ensures(Contract.Result<Task>() != null);
            var path = context.Request.Path;

            if (path != null && !string.IsNullOrEmpty(path.Value))
            {
                Configuration = configuration;
                path = path.Value.ToLower();

                if (!IsNoCheckAuthorization(configuration, path))
                {
                    if (path.StartsWithSegments(new PathString("/v1")))
                    {
                        if (string.IsNullOrEmpty(context.Request.Headers["Authorization"]))
                        {
                            context.Response.ContentType = "application/json";
                            context.Response.StatusCode = 403; //Bad Request              
                            await context.Response.WriteAsync(
                                JsonConvert.SerializeObject(
                                    MiddlewareResult.Forbidden()));
                            return;
                        }
                        else
                        {
                            var authorization = context.Request.Headers["Authorization"].ToString();

                            if (!authorization.ToLower().StartsWith("bearer", StringComparison.Ordinal))
                            {
                                context.Response.ContentType = "application/json";
                                context.Response.StatusCode = 403; //Bad Request              
                                await context.Response.WriteAsync(
                                    JsonConvert.SerializeObject(
                                        MiddlewareResult.Forbidden()));
                                return;
                            }
                            else
                            {
                                var token = authorization.Replace("bearer ", "").Trim();
                                token = token.Replace("Bearer ", "").Trim();

                                if (string.IsNullOrEmpty(token))
                                {
                                    context.Response.ContentType = "application/json";
                                    context.Response.StatusCode = 403; //Bad Request              
                                    await context.Response.WriteAsync(
                                        JsonConvert.SerializeObject(
                                            MiddlewareResult.Forbidden()));
                                    return;
                                }
                                else
                                {
                                    string[] jwt = token.Split(new char[] { '.' });

                                    if (!jwt.Length.Equals(3))
                                    {
                                        context.Response.ContentType = "application/json";
                                        context.Response.StatusCode = 403; //Bad Request              
                                        await context.Response.WriteAsync(
                                            JsonConvert.SerializeObject(
                                                MiddlewareResult.Forbidden()));
                                        return;
                                    }
                                    else
                                    {
                                        var payloadJson = CryptoServices.Base64Decode(jwt[1]);
                                        Payload payload = JsonConvert.DeserializeObject<Payload>(payloadJson);

                                        if (!payload.IsConfirmed)
                                        {
                                            context.Response.ContentType = "application/json";
                                            context.Response.StatusCode = 403; //Bad Request              
                                            await context.Response.WriteAsync(
                                                JsonConvert.SerializeObject(
                                                    MiddlewareResult.Forbidden()));
                                            return;

                                        }
                                        else
                                        {
                                            //--> Verificando se token expirou
                                            if (payload.Exp.Subtract(DateTime.Now).Days <= 0)
                                            {
                                                context.Response.ContentType = "application/json";
                                                context.Response.StatusCode = 401; //Bad Request              
                                                await context.Response.WriteAsync(
                                                    JsonConvert.SerializeObject(
                                                        MiddlewareResult.Unauthorized()));
                                                return;
                                            }
                                            else
                                            {
                                                //--> Validar assinatura do token (RSA) 
                                                if (!IsValidSign(jwt))
                                                {
                                                    context.Response.ContentType = "application/json";
                                                    context.Response.StatusCode = 401; //Bad Request              
                                                    await context.Response.WriteAsync(
                                                        JsonConvert.SerializeObject(
                                                            MiddlewareResult.Unauthorized()));
                                                    return;
                                                }
                                                else
                                                {
                                                    ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal();
                                                    claimsPrincipal.AddIdentity(new ClaimsIdentity(new[]
                                                    {
                                                        new Claim(ClaimTypes.Authentication, "true"),
                                                        new Claim(ClaimTypes.Sid, payload.AccountID.ToString()),
                                                        new Claim(ClaimTypes.Name, payload.Name),
                                                        new Claim(ClaimTypes.Email, payload.Email),
                                                        new Claim(ClaimTypes.GroupSid, payload.CompanyID.ToString()),
                                                        new Claim(ClaimTypes.Expiration, payload.Exp.Ticks.ToString()),
                                                        new Claim("IsConfirmed", payload.IsConfirmed.ToString()),
                                                        new Claim("CompanyID", payload.CompanyID.ToString()),
                                                        new Claim("GroupID", payload.GroupID.ToString())
                                                    }));
                                                    /* add roles
                                                        new Claim(ClaimTypes.Role, "RDPROPONENT"),
                                                        new Claim(ClaimTypes.Role, "RWPROJECT")*/
                                                    context.User = claimsPrincipal;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //await _next.Invoke(context);
            await _next(context);
        }

        private bool IsNoCheckAuthorization(IConfiguration configuration, string pathRequest)
        {
            bool result = false;

            var urlNoCheck = configuration.GetSection("ApplicationConfig:NoCheckAuthorization").Get<string[]>();

            if (urlNoCheck != null && urlNoCheck.Length > 0)
            {
                result = urlNoCheck.Contains(pathRequest);

                //--> Check segments of url
                if (!result)
                {
                    foreach (string url in urlNoCheck)
                    {
                        if (url.Contains("{"))
                        {
                            result = CheckSegmentUrl(url.Split('/'), pathRequest.Split('/'));
                        }
                    }
                }
            }

            return result;
        }

        private bool CheckSegmentUrl(string[] urlNoCheck, string[] urlRequest)
        {
            bool result = false;
            int relevance = 0;

            if (urlRequest != null && urlRequest.Length > 0)
            {
                if (urlNoCheck.Length.Equals(urlRequest.Length))
                {
                    int i = 0;
                    foreach (string url in urlNoCheck)
                    {
                        if (url.Equals(urlRequest[i]) || url.Equals(string.Empty) || (url.StartsWith("{", StringComparison.Ordinal) && url.EndsWith("}", StringComparison.Ordinal)))
                        {
                            relevance++;
                        }

                        i++;
                    }

                    //--> Calc relevance
                    if (relevance.Equals(urlNoCheck.Length))
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        private bool IsValidSign(string[] jwt)
        {
            bool result = true;

            RSAParametersJson key = Configuration.GetSection("RSAKey").Get<RSAParametersJson>();
            var clearText = string.Format("{0}.{1}", jwt[0], jwt[1]);
            //var cipherText = proxyServices.CryptoServices.RSAEncrypt(clearText, key);
            result = proxyServices.CryptoServices.RSAIsValidSigned(clearText, jwt[2], key);

            return result;
        }
    }
}
