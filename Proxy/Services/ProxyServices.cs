﻿using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Eprocurement.Services;
using FS.Libraries.Security.CryptoSecurity.Services;
using FS.Libraries.Tools.SendMail.Data;
using System;


namespace FS.Libraries.Proxy.Services
{
    public class ProxyServices : IProxyServices
    {
        private EprocurementContext _eprocurementContext = null;

        private EmailConfiguration _emailConfiguration { get; set; }

        public IEprocurementServices Eprocurement => new EprocurementActionsServices(_eprocurementContext);

        public ICryptoServices CryptoServices => new CryptoServices();

        public ProxyServices() { }

        public ProxyServices(EprocurementContext eprocurementContext)
        {
            _eprocurementContext = eprocurementContext;
        }
        public ProxyServices(EprocurementContext eprocurementContext, EmailConfiguration emailConfiguration)
        {
            _eprocurementContext = eprocurementContext;
            _emailConfiguration = emailConfiguration;
        }
    }
}
