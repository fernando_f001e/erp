﻿using FS.Libraries.Eprocurement.Services;
using FS.Libraries.Security.CryptoSecurity.Services;
using System;

namespace FS.Libraries.Proxy.Services
{
    public interface IProxyServices
    {
        /// <summary>
        /// Get service library for Core Solution
        /// </summary>
        IEprocurementServices Eprocurement { get; }

        /// <summary>
        ///  Service library for Security Solution
        /// </summary>
        ICryptoServices CryptoServices { get; }
    }
}
