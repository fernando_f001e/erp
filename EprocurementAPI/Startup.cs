using System;
using System.IO;
using System.Reflection;
using Amazon.SimpleEmail.Model;
using FS.Libraries.Eprocurement.Context;
using FS.Libraries.Proxy.Middleware;
using FS.WebAPI.EprocurementAPI.Controllers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace EprocurementAPI
{
    public class Startup
    {
        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            HostingEnvironment = environment;
            Configuration = configuration;
        }

        public IWebHostEnvironment HostingEnvironment { get; set; }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            string nameMachineName = Environment.MachineName;

            if (HostingEnvironment.IsDevelopment() && !string.IsNullOrEmpty(nameMachineName))
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<EprocurementContext>(options => options.UseNpgsql(Configuration.GetConnectionString(nameMachineName.ToUpper())));
            }
            else
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<EprocurementContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            }

            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);

            //services.AddRazorPages();

            //services.AddOptions();
            //services.AddControllers();

            //services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            //app.UseStaticFiles();

            LogLevel level = Configuration.GetSection("Logging:LogLevel").GetValue<LogLevel>("Default");

            SetLogger(level);

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            app.UseDefaultFiles();
            app.UseRouting();

            app.UseAuthentication();

            app.UseMiddleware<AuthorizationMiddleware>();

            RunMigrationUpdateDatabase(app);

            app.UseDeveloperExceptionPage();

            //app.UseFileServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                name: "start",
                pattern: "Start/Index",
                defaults: new { controller = "Start", action = "Index" });
            });

            //app.UseHttpsRedirection();
            //app.UseRouting();
            //app.UseAuthorization();
            //app.UseMvc(routes => { routes.MapRoute("default", "api/{controller=Start}/{action=Index}"); });
        }

        private void SetLogger(LogLevel level)
        {
            if (HostingEnvironment.IsDevelopment())
            {
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.FromLogContext()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile(@"Logs/EprocurementAPI.log", retainedFileCountLimit: 7)
                    .WriteTo.Seq("http://localhost:5341")
                    .CreateLogger();
            }
            else
            {
                if (level.Equals(LogLevel.Debug))
                {
                    Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.FromLogContext()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile(@"Logs/EprocurementAPI.log", retainedFileCountLimit: 7)
                    .WriteTo.Seq("http://localhost:5341")
                    .CreateLogger();
                }
                else
                {
                    Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Error()
                        .Enrich.FromLogContext()
                        .WriteTo.LiterateConsole()
                        .WriteTo.RollingFile(@"Logs/EprocurementAPI.log", retainedFileCountLimit: 7)
                        .WriteTo.Seq("http://localhost:5341")
                        .CreateLogger();
                }
            }
        }

        private void RunMigrationUpdateDatabase(IApplicationBuilder app)
        {
            try
            {
                bool migrationsUpdate = Configuration.GetSection("ApplicationConfig").GetValue<bool>("MigrationsDatabaseUpdate");

                if (migrationsUpdate)
                {
                    using IServiceScope serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
                    var contextCore = serviceScope.ServiceProvider.GetService<EprocurementContext>();
                    contextCore.Database.Migrate();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Log.Error("Error Runs the migration update database.", ex);
            }
        }
    }
}
