﻿using FS.WebAPI.EprocurementAPI.Helper;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace FS.WebAPI.EprocurementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StartController : Controller
    {
        [Route("Index")]
        public IActionResult Index()
        {
            ViewData["version"] = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            return View(PathViews.Login);
        }
    }
}
